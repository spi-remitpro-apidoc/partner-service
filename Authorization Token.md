# Remitpro - API Partner Doc. [Authorization]

|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/authentication-server/oauth/token`|
|Production|`https://apigw.kaspro.id/authentication-server/oauth/token`|

**Header**
> client_id & client_secret generate from remitpro

|Key |Value |Desc  |
|-|-|-|
|Authorization|`Basic Base64({client_id}:{client_secret})`||
|Content-Type |`application/x-www-form-urlencoded`||

**Request Body**

|Field|Value|Desc|
|-|-|-|
|grant_type|`client_credentials`||

**Response Success**
```
{
    "access_token": "eyJvcmciOiI1ZjY2YzEyMzMxODYwMjAwMDFmN2U0YjMiLCJpZCI6Ijg4ZTAwNWQzNDNmOTQ4YWI5ZTgzMjhjMjc2NzY0NDJiIiwiaCI6Im11cm11cjY0In0=",
    "expires_in": 3600,
    "token_type": "bearer"
}
```
**Response Error**
```
{
    "error": "invalid_request",
    "error_description": "The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed."
}
```
