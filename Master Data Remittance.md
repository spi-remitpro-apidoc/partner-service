# Remitpro - API Partner Doc. [Master Data Remittance]


|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Master Data Remittance
### Source Of Funds
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/source-of-funds/{type}`|Enum Value type: `REMITPRO` or `WESTERN_UNION`|

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 2,
            "name": "Savings",
            "vendorName": "REMITPRO",
            "code": null,
            "createdAt": "2021-02-14T15:05:38.130Z",
            "updatedAt": "2021-02-14T15:05:38.130Z"
        },
        {
            "id": 3,
            "name": "Gift",
            "vendorName": "REMITPRO",
            "code": null,
            "createdAt": "2021-02-14T15:05:38.139Z",
            "updatedAt": "2021-02-14T15:05:38.139Z"
        },
        {
            "id": 4,
            "name": "Pension or Government or Welfare",
            "vendorName": "REMITPRO",
            "code": null,
            "createdAt": "2021-02-14T15:05:38.149Z",
            "updatedAt": "2021-02-14T15:05:38.149Z"
        },
        {
            "id": 5,
            "name": "Inheritance",
            "vendorName": "REMITPRO",
            "code": null,
            "createdAt": "2021-02-14T15:05:38.162Z",
            "updatedAt": "2021-02-14T15:05:38.162Z"
        },
        {
            "id": 1,
            "name": "Salary or Income",
            "vendorName": "REMITPRO",
            "code": null,
            "createdAt": "2021-02-14T14:53:49.773Z",
            "updatedAt": "2021-02-14T14:53:49.773Z"
        },
        {
            "id": 11,
            "name": "Borrowed Funds / Loan",
            "vendorName": "REMITPRO",
            "code": null,
            "createdAt": "2021-02-15T04:46:27.588Z",
            "updatedAt": "2021-02-15T04:46:27.588Z"
        },
        {
            "id": 12,
            "name": "Saving./ Investment",
            "vendorName": "REMITPRO",
            "code": null,
            "createdAt": "2021-02-15T10:43:23.342Z",
            "updatedAt": "2021-02-15T10:43:23.342Z"
        }
    ],
    "response_timestamp": "2021-08-02T13:07:18.116+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Purpose Of Funds
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/purpose-of-funds/{type}`|Enum Value type: `REMITPRO` or `WESTERN_UNION`|

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 9,
            "name": "Family Support/Living Expenses",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.043Z",
            "updatedAt": "2021-02-14T15:11:02.043Z"
        },
        {
            "id": 10,
            "name": "Saving/Investments",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.052Z",
            "updatedAt": "2021-02-14T15:11:02.052Z"
        },
        {
            "id": 11,
            "name": "Gift",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.061Z",
            "updatedAt": "2021-02-14T15:11:02.061Z"
        },
        {
            "id": 12,
            "name": "Goods & Services payment/Commercial Transaction",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.071Z",
            "updatedAt": "2021-02-14T15:11:02.071Z"
        },
        {
            "id": 13,
            "name": "Travel expenses",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.076Z",
            "updatedAt": "2021-02-14T15:11:02.076Z"
        },
        {
            "id": 14,
            "name": "Education/School Fee",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.087Z",
            "updatedAt": "2021-02-14T15:11:02.087Z"
        },
        {
            "id": 15,
            "name": "Rent/Mortgage",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.095Z",
            "updatedAt": "2021-02-14T15:11:02.095Z"
        },
        {
            "id": 16,
            "name": "Emergency/Medical Aid",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.103Z",
            "updatedAt": "2021-02-14T15:11:02.103Z"
        },
        {
            "id": 17,
            "name": "Employee Payroll/Employee Expense",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.111Z",
            "updatedAt": "2021-02-14T15:11:02.111Z"
        }
    ],
    "response_timestamp": "2021-08-02T13:08:23.358+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Id Type Data
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/id-type-data`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "code": "KTP",
            "name": "KTP"
        },
        {
            "code": "Government Issued Int'l ID",
            "name": "Government Issued Int'l ID"
        },
        {
            "code": "Government Issued Nat'l ID",
            "name": "Government Issued Nat'l ID"
        }
    ],
    "response_timestamp": "2021-08-02T13:09:50.028+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Remit Type Data
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/remit-type-data`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "code": "CASH_TO_CASH_REMITPRO",
            "name": "CASH_TO_CASH_REMITPRO"
        },
        {
            "code": "CASH_TO_CASH_WESTERN_UNION",
            "name": "CASH_TO_CASH_WESTERN_UNION"
        },
        {
            "code": "CASH_TO_CASH_XPRESS_MONEY",
            "name": "CASH_TO_CASH_XPRESS_MONEY"
        },
        {
            "code": "CASH_TO_ACCOUNT_XPRESS_MONEY",
            "name": "CASH_TO_ACCOUNT_XPRESS_MONEY"
        },
        {
            "code": "CASH_TO_ACCOUNT_TPT",
            "name": "CASH_TO_ACCOUNT_TPT"
        },
        {
            "code": "CASH_TO_ACCOUNT",
            "name": "CASH_TO_ACCOUNT"
        },
        {
            "code": "CASH_TO_POS",
            "name": "CASH_TO_POS"
        },
        {
            "code": "WALLET_TO_CASH",
            "name": "WALLET_TO_CASH"
        },
        {
            "code": "MERCHANT_TRADE",
            "name": "MERCHANT_TRADE"
        }
    ],
    "response_timestamp": "2021-08-02T13:10:49.492+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Relation Of Receive
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/relation-of-receive`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 1,
            "name": "Family",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:12:03.913Z",
            "updatedAt": "2021-02-14T15:12:03.913Z"
        },
        {
            "id": 2,
            "name": "Friend",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:12:03.921Z",
            "updatedAt": "2021-02-14T15:12:03.921Z"
        },
        {
            "id": 3,
            "name": "Trade/BusinessPartner",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:12:03.930Z",
            "updatedAt": "2021-02-14T15:12:03.930Z"
        },
        {
            "id": 4,
            "name": "Employee/Employer",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:12:03.938Z",
            "updatedAt": "2021-02-14T15:12:03.938Z"
        },
        {
            "id": 5,
            "name": "Purchase/Seller",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:12:03.946Z",
            "updatedAt": "2021-02-14T15:12:03.946Z"
        }
    ],
    "response_timestamp": "2021-08-02T13:11:31.489+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Banks
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/banks`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 1,
            "vendorName": "WESTERN_UNION",
            "bankName": "BCA",
            "bankCode": "BCA",
            "country": "ID",
            "createdAt": "2021-02-14T13:07:52.633Z",
            "updatedAt": "2021-02-14T13:07:52.633Z"
        },
        {
            "id": 2,
            "vendorName": "WESTERN_UNION",
            "bankName": "ABN Amro",
            "bankCode": "ABN Amro",
            "country": "ID",
            "createdAt": "2021-02-14T15:33:24.326Z",
            "updatedAt": "2021-02-14T15:33:24.326Z"
        },
        {
            "id": 3,
            "vendorName": "WESTERN_UNION",
            "bankName": "Agroniaga",
            "bankCode": "Agroniaga",
            "country": "ID",
            "createdAt": "2021-02-14T15:33:24.334Z",
            "updatedAt": "2021-02-14T15:33:24.334Z"
        }
    ],
    "response_timestamp": "2021-08-02T13:11:31.489+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Employment Indsutry
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/employment-industries`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 1,
            "name": "Advertising/Media/Communications",
            "nameId": "Iklan/Media/Komunikasi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.164Z",
            "updatedAt": "2021-02-14T15:15:59.164Z"
        },
        {
            "id": 3,
            "name": "Agriculture/Manufacturing",
            "nameId": "Pertanian/Manufaktur",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.194Z",
            "updatedAt": "2021-02-14T15:15:59.194Z"
        },
        {
            "id": 4,
            "name": "Banking/Finance/Insurance",
            "nameId": "Perbankan/Keuangan/Asuransi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.201Z",
            "updatedAt": "2021-02-14T15:15:59.201Z"
        },
        {
            "id": 5,
            "name": "Construction",
            "nameId": "Konstruksi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.210Z",
            "updatedAt": "2021-02-14T15:15:59.210Z"
        },
        {
            "id": 6,
            "name": "Government Civil Servant",
            "nameId": "Pegawai Negeri Sipil",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.219Z",
            "updatedAt": "2021-02-14T15:15:59.219Z"
        },
        {
            "id": 7,
            "name": "Personal Care Services",
            "nameId": "Layanan Perawatan Pribadi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.228Z",
            "updatedAt": "2021-02-14T15:15:59.228Z"
        },
        {
            "id": 8,
            "name": "Domestic Helper/Child Care",
            "nameId": "Pembantu Rumah Tangga/Penitipan Anak",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.236Z",
            "updatedAt": "2021-02-14T15:15:59.236Z"
        },
        {
            "id": 9,
            "name": "Education/Teaching",
            "nameId": "Pendidikan/Pengajaran",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.245Z",
            "updatedAt": "2021-02-14T15:15:59.245Z"
        },
        {
            "id": 10,
            "name": "Hotel/Restaurant",
            "nameId": "Hotel/Restoran",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.256Z",
            "updatedAt": "2021-02-14T15:15:59.256Z"
        },
        {
            "id": 11,
            "name": "Information Technology",
            "nameId": "Teknologi Informasi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.265Z",
            "updatedAt": "2021-02-14T15:15:59.265Z"
        },
        {
            "id": 12,
            "name": "Transportation/Shipping",
            "nameId": "Transportasi/Pengiriman",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.274Z",
            "updatedAt": "2021-02-14T15:15:59.274Z"
        },
        {
            "id": 13,
            "name": "Military/Law Enforcement",
            "nameId": "Militer/Penegak Hukum",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.288Z",
            "updatedAt": "2021-02-14T15:15:59.288Z"
        },
        {
            "id": 14,
            "name": "Automotive Repairs and Sales",
            "nameId": "Perbaikan dan Penjualan Otomotif",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.305Z",
            "updatedAt": "2021-02-14T15:15:59.305Z"
        },
        {
            "id": 15,
            "name": "Medical & Health",
            "nameId": "Kesehatan medis",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.312Z",
            "updatedAt": "2021-02-14T15:15:59.312Z"
        },
        {
            "id": 16,
            "name": "Real Estate/Property",
            "nameId": "Real Estat/Properti",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.320Z",
            "updatedAt": "2021-02-14T15:15:59.320Z"
        },
        {
            "id": 17,
            "name": "Retail/Sales",
            "nameId": "Penjualan eceran",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.328Z",
            "updatedAt": "2021-02-14T15:15:59.328Z"
        },
        {
            "id": 18,
            "name": "Tourism/Airlines/Maritime",
            "nameId": "Pariwisata/Maskapai Penerbangan/Maritim",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.337Z",
            "updatedAt": "2021-02-14T15:15:59.337Z"
        },
        {
            "id": 19,
            "name": "Arts/Entertainment and Recreation",
            "nameId": "Seni/Hiburan dan Rekreasi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.345Z",
            "updatedAt": "2021-02-14T15:15:59.345Z"
        },
        {
            "id": 20,
            "name": "Mining/Oil and Gas/Energy",
            "nameId": "Pertambangan/Minyak dan Gas/Energi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.357Z",
            "updatedAt": "2021-02-14T15:15:59.357Z"
        },
        {
            "id": 21,
            "name": "Professional Services/Legal",
            "nameId": "Layanan Profesional/Hukum",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.365Z",
            "updatedAt": "2021-02-14T15:15:59.365Z"
        },
        {
            "id": 22,
            "name": "Water/Utilities/Waste Management",
            "nameId": "Air/Utilitas/Pengelolaan Limbah",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.372Z",
            "updatedAt": "2021-02-14T15:15:59.372Z"
        },
        {
            "id": 23,
            "name": "Science",
            "nameId": "Sains",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.382Z",
            "updatedAt": "2021-02-14T15:15:59.382Z"
        }
    ],
    "response_timestamp": "2022-05-24T07:37:33.855+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Employment Position Level
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/employment-position-levels`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 1,
            "name": "Advertising/Media/Communications",
            "nameId": "Iklan/Media/Komunikasi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.164Z",
            "updatedAt": "2021-02-14T15:15:59.164Z"
        },
        {
            "id": 3,
            "name": "Agriculture/Manufacturing",
            "nameId": "Pertanian/Manufaktur",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.194Z",
            "updatedAt": "2021-02-14T15:15:59.194Z"
        },
        {
            "id": 4,
            "name": "Banking/Finance/Insurance",
            "nameId": "Perbankan/Keuangan/Asuransi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.201Z",
            "updatedAt": "2021-02-14T15:15:59.201Z"
        },
        {
            "id": 5,
            "name": "Construction",
            "nameId": "Konstruksi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.210Z",
            "updatedAt": "2021-02-14T15:15:59.210Z"
        },
        {
            "id": 6,
            "name": "Government Civil Servant",
            "nameId": "Pegawai Negeri Sipil",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.219Z",
            "updatedAt": "2021-02-14T15:15:59.219Z"
        },
        {
            "id": 7,
            "name": "Personal Care Services",
            "nameId": "Layanan Perawatan Pribadi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.228Z",
            "updatedAt": "2021-02-14T15:15:59.228Z"
        },
        {
            "id": 8,
            "name": "Domestic Helper/Child Care",
            "nameId": "Pembantu Rumah Tangga/Penitipan Anak",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.236Z",
            "updatedAt": "2021-02-14T15:15:59.236Z"
        },
        {
            "id": 9,
            "name": "Education/Teaching",
            "nameId": "Pendidikan/Pengajaran",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.245Z",
            "updatedAt": "2021-02-14T15:15:59.245Z"
        },
        {
            "id": 10,
            "name": "Hotel/Restaurant",
            "nameId": "Hotel/Restoran",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.256Z",
            "updatedAt": "2021-02-14T15:15:59.256Z"
        },
        {
            "id": 11,
            "name": "Information Technology",
            "nameId": "Teknologi Informasi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.265Z",
            "updatedAt": "2021-02-14T15:15:59.265Z"
        },
        {
            "id": 12,
            "name": "Transportation/Shipping",
            "nameId": "Transportasi/Pengiriman",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.274Z",
            "updatedAt": "2021-02-14T15:15:59.274Z"
        },
        {
            "id": 13,
            "name": "Military/Law Enforcement",
            "nameId": "Militer/Penegak Hukum",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.288Z",
            "updatedAt": "2021-02-14T15:15:59.288Z"
        },
        {
            "id": 14,
            "name": "Automotive Repairs and Sales",
            "nameId": "Perbaikan dan Penjualan Otomotif",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.305Z",
            "updatedAt": "2021-02-14T15:15:59.305Z"
        },
        {
            "id": 15,
            "name": "Medical & Health",
            "nameId": "Kesehatan medis",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.312Z",
            "updatedAt": "2021-02-14T15:15:59.312Z"
        },
        {
            "id": 16,
            "name": "Real Estate/Property",
            "nameId": "Real Estat/Properti",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.320Z",
            "updatedAt": "2021-02-14T15:15:59.320Z"
        },
        {
            "id": 17,
            "name": "Retail/Sales",
            "nameId": "Penjualan eceran",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.328Z",
            "updatedAt": "2021-02-14T15:15:59.328Z"
        },
        {
            "id": 18,
            "name": "Tourism/Airlines/Maritime",
            "nameId": "Pariwisata/Maskapai Penerbangan/Maritim",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.337Z",
            "updatedAt": "2021-02-14T15:15:59.337Z"
        },
        {
            "id": 19,
            "name": "Arts/Entertainment and Recreation",
            "nameId": "Seni/Hiburan dan Rekreasi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.345Z",
            "updatedAt": "2021-02-14T15:15:59.345Z"
        },
        {
            "id": 20,
            "name": "Mining/Oil and Gas/Energy",
            "nameId": "Pertambangan/Minyak dan Gas/Energi",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.357Z",
            "updatedAt": "2021-02-14T15:15:59.357Z"
        },
        {
            "id": 21,
            "name": "Professional Services/Legal",
            "nameId": "Layanan Profesional/Hukum",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.365Z",
            "updatedAt": "2021-02-14T15:15:59.365Z"
        },
        {
            "id": 22,
            "name": "Water/Utilities/Waste Management",
            "nameId": "Air/Utilitas/Pengelolaan Limbah",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.372Z",
            "updatedAt": "2021-02-14T15:15:59.372Z"
        },
        {
            "id": 23,
            "name": "Science",
            "nameId": "Sains",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:15:59.382Z",
            "updatedAt": "2021-02-14T15:15:59.382Z"
        }
    ],
    "response_timestamp": "2022-05-24T07:37:33.855+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Employment Status
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/employment-status`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 2,
            "name": "Employed",
            "nameId": "Bekerja",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:18:08.407Z",
            "updatedAt": "2021-02-14T15:18:08.407Z"
        },
        {
            "id": 3,
            "name": "Unemployed",
            "nameId": "Tidak Bekerja",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:18:08.417Z",
            "updatedAt": "2021-02-14T15:18:08.417Z"
        },
        {
            "id": 4,
            "name": "Student",
            "nameId": "Pelajar",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:18:08.425Z",
            "updatedAt": "2021-02-14T15:18:08.425Z"
        },
        {
            "id": 5,
            "name": "Retired",
            "nameId": "Pensiun",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:18:08.434Z",
            "updatedAt": "2021-02-14T15:18:08.434Z"
        },
        {
            "id": 6,
            "name": "Self-Employed",
            "nameId": "Bekerja sendiri",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:18:08.443Z",
            "updatedAt": "2021-02-14T15:18:08.443Z"
        }
    ],
    "response_timestamp": "2022-05-24T07:42:58.672+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Payment Source
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/payment-source`|`?type={transaction-type}` is required|

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "code": "VA",
            "name": "VIRTUAL ACCOUNT"
        },
        {
            "code": "RETAIL",
            "name": "RETAIL TPT"
        }
    ],
    "response_timestamp": "2021-10-08T06:13:02.810+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
