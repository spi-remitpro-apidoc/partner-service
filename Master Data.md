# Remitpro - API Partner Doc. [Master Data Wilayah]


|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Master Data
### Countries
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/master/countries`|-|

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 1,
            "code": "ID",
            "name": "Indonesia",
            "active": true,
            "currencyCode": "IDR",
            "createdAt": "2021-01-26T01:28:55.532Z",
            "updatedAt": "2021-01-26T01:28:55.532Z"
        },
        {
            "id": 2,
            "code": "NZ",
            "name": "New Zealand",
            "active": true,
            "currencyCode": "NZD",
            "createdAt": "2021-01-26T01:28:55.532Z",
            "updatedAt": "2021-01-26T01:28:55.532Z"
        },
    ],
    "response_timestamp": "2021-07-28T05:23:29.756+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
### Province
|METHOD |PATH |DESC  |
|-|-|-|
|GET|`/api/master/province/{countryId}`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 21,
            "CountryId": 1,
            "name": "Aceh",
            "active": true,
            "createdAt": "2021-01-26T01:28:55.564Z",
            "updatedAt": "2021-01-26T01:28:55.564Z",
            "Country": {
                "id": 1,
                "code": "ID",
                "name": "Indonesia",
                "active": true,
                "currencyCode": "IDR",
                "createdAt": "2021-01-26T01:28:55.532Z",
                "updatedAt": "2021-01-26T01:28:55.532Z"
            }
        },
        {
            "id": 1,
            "CountryId": 1,
            "name": "Bali",
            "active": true,
            "createdAt": "2021-01-26T01:28:55.564Z",
            "updatedAt": "2021-01-26T01:28:55.564Z",
            "Country": {
                "id": 1,
                "code": "ID",
                "name": "Indonesia",
                "active": true,
                "currencyCode": "IDR",
                "createdAt": "2021-01-26T01:28:55.532Z",
                "updatedAt": "2021-01-26T01:28:55.532Z"
            }
        },
    ],
    "response_timestamp": "2021-07-28T05:32:14.771+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### City
|METHOD |PATH |DESC  |
|-|-|-|
|GET|`/api/master/cities/{provinceId}`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 15,
            "ProvinceId": 2,
            "name": "Bangka",
            "active": true,
            "createdAt": "2021-01-26T01:28:55.570Z",
            "updatedAt": "2021-01-26T01:28:55.570Z",
            "Province": {
                "id": 2,
                "CountryId": 1,
                "name": "Bangka Belitung",
                "active": true,
                "createdAt": "2021-01-26T01:28:55.564Z",
                "updatedAt": "2021-01-26T01:28:55.564Z",
                "Country": {
                    "id": 1,
                    "code": "ID",
                    "name": "Indonesia",
                    "active": true,
                    "currencyCode": "IDR",
                    "createdAt": "2021-01-26T01:28:55.532Z",
                    "updatedAt": "2021-01-26T01:28:55.532Z"
                }
            }
        },
    ],
    "response_timestamp": "2021-07-28T05:32:14.771+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Sub Urb
|METHOD |PATH |DESC  |
|-|-|-|
|GET|`/api/master/suburbs/{cityId}`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 72,
            "CityId": 12,
            "name": "Kelapa Kampit",
            "active": true,
            "createdAt": "2021-01-26T01:28:55.619Z",
            "updatedAt": "2021-01-26T01:28:55.619Z",
            "City": {
                "id": 12,
                "ProvinceId": 2,
                "name": "Belitung Timur",
                "active": true,
                "createdAt": "2021-01-26T01:28:55.570Z",
                "updatedAt": "2021-01-26T01:28:55.570Z",
                "Province": {
                    "id": 2,
                    "CountryId": 1,
                    "name": "Bangka Belitung",
                    "active": true,
                    "createdAt": "2021-01-26T01:28:55.564Z",
                    "updatedAt": "2021-01-26T01:28:55.564Z",
                    "Country": {
                        "id": 1,
                        "code": "ID",
                        "name": "Indonesia",
                        "active": true,
                        "currencyCode": "IDR",
                        "createdAt": "2021-01-26T01:28:55.532Z",
                        "updatedAt": "2021-01-26T01:28:55.532Z"
                    }
                }
            }
        },
    ],
    "response_timestamp": "2021-07-28T05:32:14.771+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Area
|METHOD |PATH |DESC  |
|-|-|-|
|GET|`/api/master/areas/{suburbId}`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 831,
            "SuburbId": 72,
            "name": "Senyubuk",
            "postCode": "33571",
            "active": true,
            "createdAt": "2021-01-26T01:28:56.002Z",
            "updatedAt": "2021-01-26T01:28:56.002Z",
            "Suburb": {
                "id": 72,
                "CityId": 12,
                "name": "Kelapa Kampit",
                "active": true,
                "createdAt": "2021-01-26T01:28:55.619Z",
                "updatedAt": "2021-01-26T01:28:55.619Z",
                "City": {
                    "id": 12,
                    "ProvinceId": 2,
                    "name": "Belitung Timur",
                    "active": true,
                    "createdAt": "2021-01-26T01:28:55.570Z",
                    "updatedAt": "2021-01-26T01:28:55.570Z",
                    "Province": {
                        "id": 2,
                        "CountryId": 1,
                        "name": "Bangka Belitung",
                        "active": true,
                        "createdAt": "2021-01-26T01:28:55.564Z",
                        "updatedAt": "2021-01-26T01:28:55.564Z",
                        "Country": {
                            "id": 1,
                            "code": "ID",
                            "name": "Indonesia",
                            "active": true,
                            "currencyCode": "IDR",
                            "createdAt": "2021-01-26T01:28:55.532Z",
                            "updatedAt": "2021-01-26T01:28:55.532Z"
                        }
                    }
                }
            }
        },
    ],
    "response_timestamp": "2021-07-28T05:32:14.771+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
