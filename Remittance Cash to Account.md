# Remitpro - API Partner Doc. [Remittance Cash to Account]

|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Remittance Cash to Bank
### Inquiry Bank
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-account/inquiry-account`|-|

#### Request
|field |status |DESC  |
|----------------|-|-|
|type|required||
|bankCode|required||
|bankAccountNumber|required||
|amount|required||
|cashier.username|required||

```
{
    "type": "CASH_TO_ACCOUNT",
    "bankCode": "BCA",
    "bankAccountNumber": "999999",
    "amount": "1000000",
    "cashier": {
        "username": "ayu.sekar"
    }
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "Method": "Inquiry",
        "ProductCode": "BCA",
        "RequestId": "caffee8b-eb1f-4772-bf85-f977e93cb266",
        "Account": "999999",
        "AccountName": "GENNY FENNELLY",
        "transctionFee": 6000,
        "discount": 0
    },
    "response_timestamp": "2021-08-07T12:13:05.536+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Create Transaction
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-account`|-|

#### Request
|field |status |DESC  |
|----------------|-|-|
|type|required||
|referenceId|required||
|bankCode|required||
|bankAccountNumber|required||
|remarks|required||
|amount|required||
|remittanceSenderId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Sender.md#remittance-sender)|
|remittanceRecipientId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Recipient.md#remittance-recipient)|
|sourceOfFundId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data%20Remittance.md#source-of-funds)|
|purposeOfFundId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data%20Remittance.md#purpose-of-funds)|
|cashier.username|required||
|callbackUrl|required||
```
{
	"type": "CASH_TO_ACCOUNT",
	"referenceId": "test_2314267831232",
	"bankCode": "BCA",
	"bankAccountNumber": "123123123",
	"remarks": "OKE",
	"amount": "100000",
	"remittanceSenderId": 222,
	"remittanceRecipientId": 675,
	"sourceOfFundId": 2,
	"purposeOfFundId": 1,
	"cashier": {
		"username": "ayu.sekar"
	},
	"callbackUrl": "https://callback.com/callback"
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
	"response_code": 0,
	"response_message": "success",
	"response_timestamp": "2021-08-02T13:20:35.315+00.00",
	"error": null,
	"response_data": {
		"number": "20210804094148",
		"code": "bf104b0a6e4ce11991af99053fad995897cff0a2e4e07c75283ada91fccc57c3",
		"digest": "b3021fab50ce0bf547be0515e6929c14",
		"type": "REMITTANCE_OUTBOUND",
		"trxRemitType": "CASH_TO_ACCOUNT",
		"remarks": "OKE",
		"currencyCode": "IDR",
		"referenceId": "6109fe699420630efcc30a27",
		"amount": 100000,
		"trxTotalAmount": 106000,
		"CashierId": 28,
		"status": "PENDING",
		"mtcn": 3450348924,
		"RemittanceSenderId": 222,
		"RemittanceRecipientId": 675,
		"PurposeOfFundId": 1,
		"SourceOfFundId": 2,
		"isRepresented": false,
		"id": 1007,
		"createdAt": "2021-08-04T02:41:48.662Z",
		"updatedAt": "2021-08-04T02:41:56.585Z",
		"trxRates": 6000,
		"vendorResponse": {
			"Price": "100000",
			"Method": "Payment",
			"ProductCode": "BCA",
			"RequestId": "515259028057",
			"Account": "123123123",
			"Amount": "100000",
			"AccountCode": "BCA",
			"TransactionId": "12312312320210804094152524",
			"Fee": "5000",
			"Time": "20210804094154691",
			"Status": "99",
			"Desc": "In Process",
			"FlowType": "Async",
			"Currency": "IDR"
		},
		"externalTransactionId": "12312312320210804094152524",
		"WalletTransactionId": 1759
	}
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}

```
### Callback Transaction
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|-|-|

#### Request
```
{
  "code": 0,
  "number": "91230122111",
  "mtcn": "123123123",
  "status": "COMPLETED",
  "description": "TRANSACTION SUCCESS"
}
```

### List Error Message Code: 999 ###
|MESSAGE|
|-------|
|You must login as cashier|
|ERROR: Failed to get cashier|
|Service fee not setup|
|bank not found!|
|reference Id: {referenceId} already use|
|Cannot transaction using this bank. please try again later|
|Cannot transaction using this bank. please try again later|
|Cannot transaction using this bank. please try again later|
|Cashier balance is not enough|
|No response from sender creation|
|No response from recipient creation|
|{any desc response BM}|
|No response from transaction creation|
|Internal Server Error|
