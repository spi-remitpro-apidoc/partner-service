# Remitpro - API Partner Doc. [Remittance Cash to Cash BRI]

|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Remittance Cash to Cash BRI
### Inquiry Remit Fee
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-cash-bri/inquiry-fee`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
```
{"type":"CASH_TO_CASH_BRI","amount":230000, "cashier":{"username":"ayu.sekar"}}
```

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "fee": 1000,
        "discount": 0,
        "finalFee": 1000
    },
    "response_timestamp": "2021-08-02T14:06:35.850+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
### Inquiry Remit Fee
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-cash-bri`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
```
{
	"type": "CASH_TO_CASH_BRI",
	"currencyCode": "IDR",
	"referenceId": "1",
	"amount": 230000,
	"remarks": "okeh",
	"remittanceSenderId": 726,
	"remittanceSenderRepresentativeId": 0,
	"remittanceRecipientId": 764,
	"isRepresented": false,
	"sourceOfFundId": 2,
	"purposeOfFundId": 3,
	"promotionCode": "",
	"originCountryId": 1,
	"originProvinceId": 2,
	"originCityId": 12,
	"originSuburbId": 78,
	"originAreaId": 858,
	"destinationCountryId": 1,
    "paymentType": "VA", //optional if using `VA`
    "cashier": {
        "username":"ayu.sekar"
    }
}
```

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "number": "20210813111946",
        "code": "ee451d5e6b1eb818c5fcc25636abcd5f91c1f1a2e5e47e752f3b8790a49a0392",
        "digest": "3fff448cac869d2229ce1135b7d3a5ee",
        "type": "REMITTANCE_OUTBOUND",
        "trxRemitType": "CASH_TO_CASH_BRI",
        "remarks": "okeh",
        "currencyCode": "IDR",
        "referenceId": "1",
        "amount": 230000,
        "trxTotalAmount": 231000,
        "CashierId": 28,
        "status": "COMPLETED",
        "mtcn": "BRITX9328651951",
        "RemittanceSenderId": 726,
        "RemittanceSenderRepresentativeId": 0,
        "RemittanceRecipientId": 764,
        "PurposeOfFundId": 3,
        "SourceOfFundId": 2,
        "isRepresented": false,
        "destinationCountryId": 1,
        "receiptNumber": "20210813111946",
        "id": 1118,
        "createdAt": "2021-08-13T04:19:46.307Z",
        "updatedAt": "2021-08-13T04:19:49.861Z",
        "trxRates": 1000,
        "vendorResponse": {
            "responseCode": "0005",
            "responseDescription": "[SUCCESS] Transaction Ready To Confirm",
            "data": {
                "ticketNumber": 23084611202113
            }
        },
        "WalletTransactionId": 1872
    },
    "response_timestamp": "2021-08-02T14:06:35.850+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
