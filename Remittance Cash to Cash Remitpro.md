# Remitpro - API Partner Doc. [Remittance Cash to Cash Remitpro]

|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Remittance Cash to Cash Remitpro
### Inquiry Remit Fee
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-remit/inquiry-fee`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
```
{
    "type": "CASH_TO_CASH_REMITPRO",
    "amount": 100000,
    "promotionCode": "",
    "originCountryId": 1,
    "originProvinceId": 2,
    "originCityId": 12,
    "originSuburbId": 78,
    "originAreaId": 858,
    "destinationCountryId": 1,
    "cashier": {
        "username": "ayu.sekar"
    }
}
```

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|
**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "fee": 1000,
        "discount": 0,
        "finalFee": 1000
    },
    "response_timestamp": "2021-08-02T14:06:35.850+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Create Transaction Remit
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-remit`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
|field|status|desc  |
|----------------|-|-|
|type|required||
|currencyCode|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#countries)|
|referenceId|required|transaction Number (unique number)|
|amount|required||
|remarks|optional||
|remittanceSenderId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Sender.md#remittance-sender)|
|remittanceRecipientId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Recipient.md#remittance-recipient)|
|sourceOfFundId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data%20Remittance.md#source-of-funds)|
|purposeOfFundId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data%20Remittance.md#purpose-of-funds)|
|isRepresented|required|set value default `false`|
|remittanceSenderRepresentativeId|required|set value default `0`|
|promotionCode|optional||
|originCountryId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#user-content-countries)|
|originProvinceId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#province)|
|originCityId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#city)|
|originSuburbId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#sub-urb)|
|originAreaId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#area)|
|destinationCountryId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#user-content-countries)|
|cashier.username|required||
|paymentType|optional|`required` if using payment type `VA`|
```
{
    "type": "CASH_TO_CASH_REMITPRO",
    "currencyCode": "IDR",
    "referenceId": "1",
    "amount": 100000,
    "remarks": "",
    "remittanceSenderId": 552,
    "remittanceSenderRepresentativeId": 0,
    "remittanceRecipientId": 592,
    "isRepresented": false,
    "sourceOfFundId": 2,
    "purposeOfFundId": 1,
    "promotionCode": "",
    "originCountryId": 1,
    "originProvinceId": 2,
    "originCityId": 12,
    "originSuburbId": 78,
    "originAreaId": 858,
    "destinationCountryId": 1,
	"paymentType": "VA", //optional if using `VA`
    "cashier": {
        "username": "ayu.sekar"
    },
}
```

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "number": "20210804091108",
        "code": "e9411f0e391ab41896a7c80438f7c85f9ac0a2a5e3b17d772c3bd696f4ce5e95",
        "digest": "4bd6f04c282d7872980b7d07a7551a8b",
        "type": "REMITTANCE_OUTBOUND",
        "trxRemitType": "CASH_TO_CASH_REMITPRO",
        "remarks": "",
        "currencyCode": "IDR",
        "referenceId": "1",
        "amount": 100000,
        "trxTotalAmount": 101000,
        "CashierId": 30,
        "status": "COMPLETED",
        "mtcn": 2491784703,
        "RemittanceSenderId": 552,
        "RemittanceSenderRepresentativeId": 0,
        "RemittanceRecipientId": 592,
        "PurposeOfFundId": 1,
        "SourceOfFundId": 2,
        "isRepresented": false,
        "destinationCountryId": 1,
        "receiptNumber": "20210804091108",
        "id": 1006,
        "createdAt": "2021-08-04T02:11:08.276Z",
        "updatedAt": "2021-08-04T02:11:10.431Z",
        "trxRates": 1000,
        "WalletTransactionId": 1758
    },
    "response_timestamp": "2021-08-04T02:11:10.463+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code":999,
    "response_message": "Error: MTCN Not Found",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null,
}
```
### Inquiry MTCN - Claim Remit
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-remit/inquiry/claim`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
```
{
    "type":"CASH_TO_CASH_REMITPRO",
    "mtcn":"100900412951",
    "cashier": {
        "username": "ayu.sekar"
    }
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|
**Response Success**
```
{
	"response_code": 0,
	"response_message": "success",
	"response_data": {
		"id": 921,
		"number": "20210725140423",
		"code": "bc1419093b4fe442c5f9ce0537facb55959da4f3b7ec7f767f34d5c1a0cd50cf",
		"digest": "a7b1ded9af4e85486e64c926286beb68",
		"voidOfTransactionId": null,
		"voidByTransactionId": null,
		"claimOfTransactionId": null,
		"claimByTransactionId": null,
		"type": "REMITTANCE_OUTBOUND",
		"trxRemitType": "CASH_TO_CASH_REMITPRO",
		"metaData": null,
		"remarks": "",
		"currencyCode": "IDR",
		"referenceId": "1",
		"promotionId": null,
		"discountAmount": null,
		"amount": "200000.00",
		"trxRates": "1000.00",
		"trxTotalAmount": "201000.00",
		"outstandingAmount": null,
		"CashierId": 28,
		"status": "COMPLETED",
		"descriptions": null,
		"vendorResponse": null,
		"mtcn": "10090041295",
		"bsmReference": null,
		"RemittanceSenderId": 549,
		"RemittanceSenderRepresentativeId": 0,
		"RemittanceRecipientId": 591,
		"RemittanceRecipientRepresentativeId": null,
		"RemittanceClaimantId": null,
		"isRepresented": false,
		"WalletTransactionId": 1708,
		"PurposeOfFundId": 2,
		"SourceOfFundId": 2,
		"receiptUrl": null,
		"createdAt": "2021-07-25T07:04:22.809Z",
		"updatedAt": "2021-07-25T07:04:29.911Z",
		"otp": null,
		"otpExpiredAt": null,
		"otpExpiryIn": null,
		"receiptNumber": "20210725140422",
		"receiptBankUrl": null,
		"WalletTransactionIds": [],
		"externalTransactionId": null,
		"destinationCountryId": 1,
		"RemittanceSender": {
			"id": 549,
			"idType": "KTP",
			"idNumber": "1231231231231231",
			"firstName": "ARIEF",
			"lastName": "HDIAYA",
			"gender": "MALE",
			"dateOfBirth": "1997-06-04",
			"placeOfBirth": "BANDUNG",
			"CountryId": 1,
			"ProvinceId": 2,
			"CityId": 12,
			"SuburbId": 78,
			"AreaId": 858,
			"postCode": "33517",
			"address": "JALAN",
			"domicileCountryId": 1,
			"domicileProvinceId": 2,
			"domicileCityId": 12,
			"domicileSuburbId": 78,
			"domicileAreaId": 858,
			"domicilePostCode": "33517",
			"domicileAddress": "JALAN",
			"phone": "6288809512095",
			"nationality": "INDONESIA",
			"bankCode": null,
			"bankAccount": null,
			"idPublicationDate": null,
			"validUntilDate": null,
			"relationToReceiver": null,
			"email": null,
			"employmentStatus": null,
			"employmentPosition": null,
			"employmentIndustry": null,
			"IMSenderIdNumber": "5c958e90-357e-4a0f-b47b-b945be27a359",
			"idCountryIssue": null,
			"createdAt": "2021-07-25T07:04:21.467Z",
			"updatedAt": "2021-07-25T07:04:21.467Z",
			"Area": {
				"id": 858,
				"SuburbId": 78,
				"name": "Kelubi",
				"postCode": "33517",
				"active": true,
				"createdAt": "2021-01-26T01:28:56.003Z",
				"updatedAt": "2021-01-26T01:28:56.003Z",
				"Suburb": {
					"id": 78,
					"CityId": 12,
					"name": "Manggar",
					"active": true,
					"createdAt": "2021-01-26T01:28:55.619Z",
					"updatedAt": "2021-01-26T01:28:55.619Z",
					"City": {
						"id": 12,
						"ProvinceId": 2,
						"name": "Belitung Timur",
						"active": true,
						"createdAt": "2021-01-26T01:28:55.570Z",
						"updatedAt": "2021-01-26T01:28:55.570Z",
						"Province": {
							"id": 2,
							"CountryId": 1,
							"name": "Bangka Belitung",
							"active": true,
							"createdAt": "2021-01-26T01:28:55.564Z",
							"updatedAt": "2021-01-26T01:28:55.564Z",
							"Country": {
								"id": 1,
								"code": "ID",
								"name": "Indonesia",
								"active": true,
								"currencyCode": "IDR",
								"createdAt": "2021-01-26T01:28:55.532Z",
								"updatedAt": "2021-01-26T01:28:55.532Z"
							}
						}
					}
				}
			},
			"domicileArea": {
				"id": 858,
				"SuburbId": 78,
				"name": "Kelubi",
				"postCode": "33517",
				"active": true,
				"createdAt": "2021-01-26T01:28:56.003Z",
				"updatedAt": "2021-01-26T01:28:56.003Z",
				"Suburb": {
					"id": 78,
					"CityId": 12,
					"name": "Manggar",
					"active": true,
					"createdAt": "2021-01-26T01:28:55.619Z",
					"updatedAt": "2021-01-26T01:28:55.619Z",
					"City": {
						"id": 12,
						"ProvinceId": 2,
						"name": "Belitung Timur",
						"active": true,
						"createdAt": "2021-01-26T01:28:55.570Z",
						"updatedAt": "2021-01-26T01:28:55.570Z",
						"Province": {
							"id": 2,
							"CountryId": 1,
							"name": "Bangka Belitung",
							"active": true,
							"createdAt": "2021-01-26T01:28:55.564Z",
							"updatedAt": "2021-01-26T01:28:55.564Z",
							"Country": {
								"id": 1,
								"code": "ID",
								"name": "Indonesia",
								"active": true,
								"currencyCode": "IDR",
								"createdAt": "2021-01-26T01:28:55.532Z",
								"updatedAt": "2021-01-26T01:28:55.532Z"
							}
						}
					}
				}
			}
		},
		"RemittanceSenderRepresentative": null,
		"RemittanceRecipient": {
			"id": 591,
			"firstName": "OAPSI",
			"lastName": "OISS",
			"gender": null,
			"dateOfBirth": null,
			"phone": null,
			"IMDestinationNumber": null,
			"idType": null,
			"idNumber": null,
			"CountryId": null,
			"ProvinceId": null,
			"CityId": null,
			"SuburbId": null,
			"AreaId": null,
			"postCode": null,
			"address": null,
			"email": null,
			"createdAt": "2021-07-25T07:04:21.843Z",
			"updatedAt": "2021-07-25T07:04:21.843Z",
			"Area": null
		},
		"RemittanceRecipientRepresentative": null,
		"PurposeOffund": {
			"id": 2,
			"name": "Goods and Services payment or Commercial Transaction",
			"vendorName": "REMITPRO",
			"code": null,
			"createdAt": "2021-02-14T15:01:40.834Z",
			"updatedAt": "2021-02-14T15:01:40.834Z"
		},
		"SourceOfFund": {
			"id": 2,
			"name": "Savings",
			"vendorName": "REMITPRO",
			"code": null,
			"createdAt": "2021-02-14T15:05:38.130Z",
			"updatedAt": "2021-02-14T15:05:38.130Z"
		},
		"hitAccountTransactions": [],
		"DestinationCountry": {
			"id": 1,
			"code": "ID",
			"name": "Indonesia",
			"active": true,
			"currencyCode": "IDR",
			"createdAt": "2021-01-26T01:28:55.532Z",
			"updatedAt": "2021-01-26T01:28:55.532Z"
		},
	},
	"response_timestamp": "2021-08-04T02:12:32.360+00.00",
	"error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "MTCN not found",
    "response_timestamp": "2021-08-04T02:14:48.624+00.00",
    "error": {
        "statusCode": 400,
        "message": "MTCN not found",
        "error": "Bad Request"
    }
}
```

### Create Transaction - Claim Remit
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-remit/claim`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
|field|status|desc  |
|----------------|-|-|
|mtcn|required||
|transactionId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Cash%20to%20Cash%20Remitpro.md#inquiry-mtcn-claim-remit)|
|claimantId|required|[Please check this documentation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Claimant.md#remittance-claimants)|
|skipOtp|required|set value default `true`|
|cashier.username|required||
```
{"mtcn":"8345987659","transactionId":1004,"claimantId":64,"skipOtp":true,"cashier":{"username":"ayu.sekar"}}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
	"response_code": 0,
	"response_message": "success",
	"response_timestamp": "2021-07-28T05:24:08.877+00.00",
	"error": null,
	"response_data": {
		{
			"id": 1005,
			"number": "1",
			"code": "1",
			"digest": "1",
			"voidOfTransactionId": null,
			"voidByTransactionId": null,
			"claimOfTransactionId": 1004,
			"claimByTransactionId": null,
			"type": "REMITTANCE_INBOUND",
			"trxRemitType": "CASH_TO_CASH_REMITPRO",
			"metaData": null,
			"remarks": null,
			"currencyCode": "IDR",
			"referenceId": null,
			"promotionId": null,
			"discountAmount": null,
			"amount": "1000.00",
			"trxRates": null,
			"trxTotalAmount": null,
			"outstandingAmount": "1000",
			"CashierId": 30,
			"status": "COMPLETED",
			"descriptions": null,
			"vendorResponse": null,
			"mtcn": "8345987659",
			"bsmReference": null,
			"RemittanceSenderId": 633,
			"RemittanceSenderRepresentativeId": null,
			"RemittanceRecipientId": 674,
			"RemittanceRecipientRepresentativeId": null,
			"RemittanceClaimantId": 64,
			"isRepresented": null,
			"WalletTransactionId": null,
			"PurposeOfFundId": null,
			"SourceOfFundId": null,
			"receiptUrl": null,
			"createdAt": "2021-08-04T02:01:09.753Z",
			"updatedAt": "2021-08-04T02:01:15.504Z",
			"otp": null,
			"otpExpiredAt": null,
			"otpExpiryIn": null,
			"receiptNumber": "20210804090109",
			"receiptBankUrl": null,
			"WalletTransactionIds": [1755, 1756, 1757],
			"externalTransactionId": null,
			"destinationCountryId": null,
			"RemittanceSender": {
				"id": 633,
				"idType": "KTP",
				"idNumber": "3213012345678901",
				"firstName": "URIP",
				"lastName": "TEST",
				"gender": "MALE",
				"dateOfBirth": "1990-06-12",
				"placeOfBirth": "SUBANG",
				"CountryId": 1,
				"ProvinceId": 9,
				"CityId": 73,
				"SuburbId": 1140,
				"AreaId": 11317,
				"postCode": "41271",
				"address": "JL. KALIJAGA",
				"domicileCountryId": 0,
				"domicileProvinceId": 0,
				"domicileCityId": 0,
				"domicileSuburbId": 0,
				"domicileAreaId": 0,
				"domicilePostCode": "",
				"domicileAddress": "",
				"phone": "082312345678",
				"nationality": "WNI",
				"bankCode": null,
				"bankAccount": null,
				"idPublicationDate": null,
				"validUntilDate": null,
				"relationToReceiver": null,
				"email": null,
				"employmentStatus": null,
				"employmentPosition": null,
				"employmentIndustry": null,
				"IMSenderIdNumber": null,
				"idCountryIssue": null,
				"createdAt": "2021-08-04T01:32:44.910Z",
				"updatedAt": "2021-08-04T01:32:44.910Z",
				"Area": {
					"id": 11317,
					"SuburbId": 1140,
					"name": "Kalijati Barat",
					"postCode": "41271",
					"active": true,
					"createdAt": "2021-01-26T01:28:56.008Z",
					"updatedAt": "2021-01-26T01:28:56.008Z",
					"Suburb": {
						"id": 1140,
						"CityId": 73,
						"name": "Kalijati",
						"active": true,
						"createdAt": "2021-01-26T01:28:55.619Z",
						"updatedAt": "2021-01-26T01:28:55.619Z",
						"City": {
							"id": 73,
							"ProvinceId": 9,
							"name": "Subang",
							"active": true,
							"createdAt": "2021-01-26T01:28:55.570Z",
							"updatedAt": "2021-01-26T01:28:55.570Z",
							"Province": {
								"id": 9,
								"CountryId": 1,
								"name": "Jawa Barat",
								"active": true,
								"createdAt": "2021-01-26T01:28:55.564Z",
								"updatedAt": "2021-01-26T01:28:55.564Z",
								"Country": {
									"id": 1,
									"code": "ID",
									"name": "Indonesia",
									"active": true,
									"currencyCode": "IDR",
									"createdAt": "2021-01-26T01:28:55.532Z",
									"updatedAt": "2021-01-26T01:28:55.532Z"
								}
							}
						}
					}
				},
				"domicileArea": null
			},
			"RemittanceSenderRepresentative": null,
			"RemittanceRecipient": {
				"id": 674,
				"firstName": "TINO",
				"lastName": "HANDIKA",
				"gender": null,
				"dateOfBirth": null,
				"phone": null,
				"IMDestinationNumber": null,
				"createdAt": "2021-08-04T01:32:45.059Z",
				"updatedAt": "2021-08-04T01:32:45.059Z"
			},
			"RemittanceRecipientRepresentative": null,
			"PurposeOffund": null,
			"SourceOfFund": null,
			"hitAccountTransactions": [],
			"DestinationCountry": null
		}
	}
}
```

**Response Error**
```
{
    "response_code":999,
    "response_message": "Error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null,
}
```
