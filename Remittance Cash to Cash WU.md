# Remitpro - API Partner Doc. [Remittance Cash to Cash WU]

|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Remittance Cash to Cash WU
### Inquiry Remit Fee
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-wu/inquiry-wu-fee`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
```
{
    "destCountryCode": "ID",
    "destCurrencyCode": "IDR",
    "amount": 100000,
    "cashier": {
        "username": "NADYA10"
    }
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|
**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "wuFee": "15,000",
        "amount": "100,000",
        "promoAmount": "0",
        "remitproFee": "0",
        "destinationPrincipalAmount": "100,000.00",
        "principalAmount": "100,000.00",
        "inquireToken": "Tt7YswvO8FVX+DMmRViINnNC+zIswrXLwLM+pTcnNgQ=",
        "totalAmount": "115,000",
        "transactionType": "WMN",
        "responseData": {
            "amount": "100,000.00",
            "phoneDeliveryAvailable": 1,
            "plusChargeAmount": "0.00",
            "payAmount": "100,000.00",
            "tolls": "0.00",
            "exchangeRate": "1.0000000",
            "charges": "15,000.00",
            "promoName": "",
            "grossTotalAmount": "115,000.00",
            "promoAmount": "0.00",
            "messageCharge": "0.00",
            "destinationPrincipalAmount": "100,000.00",
            "taxRate": "0",
            "promoCode": "",
            "securityQuestionRequired": 0,
            "moneyTransferLimit": 0
        }
    },
    "response_timestamp": "2021-08-12T05:06:47.708+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
### Create Transaction
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-wu`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
```
{
  "type": "CASH_TO_CASH_WESTERN_UNION",
  "amount": 100000,
  "remarks": "",
  "remittanceSenderId": 708,
  "remittanceRecipientId": 735,
  "sourceOfFundId": 6,
  "purposeOfFundId": 9,
  "originCountryId": 1,
  "originProvinceId": 21,
  "destinationCountryId": 1,
  "destCountryCode": "ID",
  "destCountry": "INDONESIA",
  "destCurrencyCode": "IDR",
  "originCountryCode": "ID",
  "originCountry": "INDONESIA",
  "originCurrencyCode": "IDR",
  "cashier": {
      "username": "ayu.sekar"
  }
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|
**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "number": "20210812125334",
        "code": "e9411f0e391ab41896a7c80438f7c85f9ac0a2a5e3b17d772c3bd696f4ce5e95",
        "digest": "4bd6f04c282d7872980b7d07a7551a8b",
        "type": "REMITTANCE_OUTBOUND",
        "trxRemitType": "CASH_TO_CASH_WESTERN_UNION",
        "remarks": "",
        "currencyCode": "IDR",
        "referenceId": "1",
        "amount": 100000,
        "trxTotalAmount": 115000,
        "CashierId": 30,
        "status": "COMPLETED",
        "mtcn": "7989100942",
        "RemittanceSenderId": 708,
        "RemittanceRecipientId": 735,
        "PurposeOfFundId": 9,
        "SourceOfFundId": 6,
        "destinationCountryId": 1,
        "id": 1091,
        "createdAt": "2021-08-12T05:53:34.600Z",
        "updatedAt": "2021-08-12T05:53:51.047Z",
        "trxRates": 0,
        "vendorResponse": {
            "date": "2021-08-12 12:53",
            "receiptData": {
                "wuFee": "0",
                "receiverProvince": "",
                "receiverState": "",
                "totalAmount": "100,000",
                "filingDate": "12 Aug 2021 12:53",
                "senderProvince": "Aceh",
                "messageCharge": "0.00",
                "senderCity": "Aceh Barat Daya",
                "countryTax": "0.00",
                "receiverPostal": "",
                "remitproFee": "0",
                "promoDescription": "",
                "municipalTax": "0.00",
                "receiverFirstName": "IASDP",
                "destinationCurrency": "IDR",
                "charges": "15,000.00",
                "receiverDob": "",
                "receiverCity": "",
                "grossTotalAmount": "115,000.00",
                "promoAmount": "0",
                "receiverCountryBirth": "",
                "promoCode": "",
                "originatorPrincipalAmount": "100,000.00",
                "senderFullName": "hidayat, arief",
                "senderAddress": "jalan",
                "senderCountryBirth": "INDONESIA",
                "receiverStateZip": "",
                "senderFirstName": "arief",
                "senderStateZip": "12123",
                "exchangeRate": "1.0000000",
                "senderDistrict": "Babah Rot",
                "senderMobile": "08283193129",
                "promoName": "",
                "cashierName": "LANA WIJAYA",
                "receiverCountry": "INDONESIA",
                "destinationCity": "",
                "destinationPrincipalAmount": "100,000.00",
                "receiverFullName": "OOSAD, IASDP",
                "sourceState": "",
                "receiverLastName": "OOSAD",
                "mtcn": "7989100942",
                "paidAmount": "0",
                "amount": "100,000",
                "senderCountry": "Indonesia",
                "maskedMtcn": "******0942",
                "receiverMobile": "",
                "plusChargeAmount": "0.00",
                "sourceCountry": "INDONESIA",
                "senderLastName": "hidayat",
                "receiverTelephone": "",
                "sourceCity": "",
                "senderTelephone": "",
                "senderPostal": "12123",
                "receiverAddress": "",
                "sourceCurrency": "IDR",
                "stateTax": "0.00",
                "receiverDistrict": "",
                "senderState": "Babah Rot",
                "destinationCountry": "INDONESIA",
                "senderDob": "2000-05-05",
                "remarks": ""
            },
            "code": 0,
            "walletTransactions": [
                {
                    "account": "ACCOUNT",
                    "description": null,
                    "credit": "0",
                    "debit": "100,000",
                    "balanceBefore": "100,306,050",
                    "balanceAfter": "100,206,050"
                }
            ],
            "paymentData": {
                "mtcn": "7989100942",
                "exchangeRate": "1.0000000",
                "amount": "100,000",
                "newMtcn": "2122487989100942",
                "plusChargeAmount": "0.00",
                "responseData": {
                    "mtcn": "7989100942",
                    "maskedMtcn": "******0942",
                    "plusChargeAmount": "0.00",
                    "municipalTax": "0.00",
                    "newMtcn": "2122487989100942",
                    "originatingState": "WE",
                    "exchangeRate": "1.0000000",
                    "charges": "15,000.00",
                    "stateTax": "0.00",
                    "grossTotalAmount": "115,000.00",
                    "messageCharge": "0.00",
                    "destinationPrincipalAmount": "100,000.00",
                    "originatorPrincipalAmount": "100,000.00",
                    "originatingCity": "SUBANG",
                    "countryTax": "0.00"
                }
            },
            "responseId": "1052108120000060",
            "receiptMessage": "PROOF OF REMITTANCE\r\n\r\nMTCN                 : 7989100942\r\nFiling Date:         : 2021-08-12 12:53\r\n\r\nSender:\r\nFirst Name           : arief\r\nLast Name            : hidayat\r\nAddress              : jalan\r\nDistrict             : Babah Rot\r\nProvince             : Aceh\r\nCountry of Residence : Indonesia\r\nTelephone No.        : \r\nDate of Birth        : 2000-05-05\r\nCountry of Birth     : INDONESIA\r\n\r\nReceiver:\r\nFirst Name           : IASDP\r\nLast Name            : OOSAD\r\nCity                 : \r\nDestination Country  : INDONESIA\r\nPhone Number         : \r\n\r\nCashier              : R012288  / 772926821238\r\nReceipt #            : 1052108120000060\r\nFee:\t\t\t     : Rp 0\r\nPayout Amount        : Rp 100,000\r\nPaid Amount          : Rp 100,000\r\nExchange Rate        : 1.0000000\r\nPaid Date            : 2021-08-12 12:53\r\n\r\nThank you for paying at REMITPRO",
            "message": "Success",
            "requestId": "IBAYAD7eb995f7-2de5-4858-a11a-9662e3496b9b"
        },
        "WalletTransactionId": 1854
    },
    "response_timestamp": "2021-08-12T05:53:51.098+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
### Inquiry Remit Fee
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-wu/inquiry/claim`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
```
{
    "type":"CASH_TO_CASH_WESTERN_UNION",
    "mtcn":"1752294556",
    "cashier": {
        "username": "ayu.sekar"
    }
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|
**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "id": 1091,
        "number": "20210812125334",
        "code": "e9411f0e391ab41896a7c80438f7c85f9ac0a2a5e3b17d772c3bd696f4ce5e95",
        "digest": "4bd6f04c282d7872980b7d07a7551a8b",
        "voidOfTransactionId": null,
        "voidByTransactionId": null,
        "claimOfTransactionId": null,
        "claimByTransactionId": null,
        "type": "REMITTANCE_OUTBOUND",
        "trxRemitType": "CASH_TO_CASH_WESTERN_UNION",
        "metaData": null,
        "remarks": "",
        "currencyCode": "IDR",
        "referenceId": "1",
        "promotionId": null,
        "discountAmount": null,
        "amount": "100000.00",
        "trxRates": "0.00",
        "trxTotalAmount": "115000.00",
        "outstandingAmount": null,
        "CashierId": 30,
        "status": "COMPLETED",
        "descriptions": null,
        "vendorResponse": {
            "mtcn": "7989100942",
            "maskedMtcn": "******0942",
            "receiver": {
                "address": {
                    "postalCode": "",
                    "countryCode": "ID",
                    "city": "",
                    "street": "",
                    "state": "",
                    "currencyCode": "IDR"
                },
                "contactNumberInfo": {
                    "countryCode": "",
                    "nationalNumber": ""
                },
                "contactNumber": "",
                "middleName": "",
                "firstName": "IASDP",
                "lastName": "OOSAD"
            },
            "sender": {
                "address": {
                    "postalCode": "12123",
                    "countryCode": "ID",
                    "city": "Aceh Barat Daya",
                    "street": "jalan",
                    "state": "Babah Rot",
                    "currencyCode": "IDR"
                },
                "contactNumberInfo": {
                    "countryCode": "62",
                    "nationalNumber": "8283193129"
                },
                "contactNumber": "628283193129",
                "middleName": "",
                "firstName": "ARIEF",
                "lastName": "HIDAYAT"
            },
            "transaction": {
                "expectedPayoutCity": "",
                "moneyTransferKey": "3623905078",
                "amount": "100,000.00",
                "destCountryCode": "ID",
                "payoutAmount": "100,000.00",
                "origDestCurrencyCode": "IDR",
                "newMtcn": "2122487989100942",
                "tolls": "0.00",
                "origCurrencyCode": "IDR",
                "payStatusDesc": "W/C",
                "filingDate": "08-12-21 ",
                "origDestCountryCode": "ID",
                "charges": "15,000.00",
                "exchangeRate": "1.0000000",
                "grossTotalAmount": "115,000.00",
                "origCountryCode": "ID",
                "filingTime": "0153A EDT",
                "principalAmount": "100,000.00",
                "destCurrencyCode": "IDR",
                "origCity": "SUBANGDO4",
                "expectedPayoutStateCode": "",
                "transactionType": "WMN"
            }
        },
        "mtcn": "7989100942",
        "bsmReference": null,
        "RemittanceSenderId": 708,
        "RemittanceSenderRepresentativeId": null,
        "RemittanceRecipientId": 735,
        "RemittanceRecipientRepresentativeId": null,
        "RemittanceClaimantId": null,
        "isRepresented": null,
        "WalletTransactionId": 1854,
        "PurposeOfFundId": 9,
        "SourceOfFundId": 6,
        "receiptUrl": null,
        "createdAt": "2021-08-12T05:53:34.600Z",
        "updatedAt": "2021-08-12T05:53:51.047Z",
        "otp": null,
        "otpExpiredAt": null,
        "otpExpiryIn": null,
        "receiptNumber": null,
        "receiptBankUrl": null,
        "WalletTransactionIds": [],
        "externalTransactionId": null,
        "destinationCountryId": 1,
        "RemittanceSender": {
            "id": 708,
            "idType": "Electronic National ID",
            "idNumber": "1231231231231231",
            "firstName": "arief",
            "lastName": "hidayat",
            "gender": "MALE",
            "dateOfBirth": "2000-05-05",
            "placeOfBirth": "bandung",
            "CountryId": 1,
            "ProvinceId": 21,
            "CityId": 256,
            "SuburbId": 3929,
            "AreaId": 47356,
            "postCode": "12123",
            "address": "jalan",
            "domicileCountryId": 1,
            "domicileProvinceId": 21,
            "domicileCityId": 256,
            "domicileSuburbId": 3929,
            "domicileAreaId": 47356,
            "domicilePostCode": "",
            "domicileAddress": "jalan",
            "phone": "08283193129",
            "nationality": "INDONESIA",
            "bankCode": null,
            "bankAccount": null,
            "idPublicationDate": "2018-06-08T00:00:00.000Z",
            "validUntilDate": "2024-06-27T00:00:00.000Z",
            "relationToReceiver": "Family",
            "email": "ariefhidayatsutomo@gmail.com",
            "employmentStatus": "Employed",
            "employmentPosition": "Entry Level",
            "employmentIndustry": "Agriculture/Manufacturing",
            "IMSenderIdNumber": null,
            "idCountryIssue": "Indonesia",
            "createdAt": "2021-08-12T05:10:48.803Z",
            "updatedAt": "2021-08-12T05:10:48.803Z",
            "Area": {
                "id": 47356,
                "SuburbId": 3929,
                "name": "Pantee Rakyat",
                "postCode": "23767",
                "active": true,
                "createdAt": "2021-01-26T01:28:55.996Z",
                "updatedAt": "2021-01-26T01:28:55.996Z",
                "Suburb": {
                    "id": 3929,
                    "CityId": 256,
                    "name": "Babah Rot",
                    "active": true,
                    "createdAt": "2021-01-26T01:28:55.619Z",
                    "updatedAt": "2021-01-26T01:28:55.619Z",
                    "City": {
                        "id": 256,
                        "ProvinceId": 21,
                        "name": "Aceh Barat Daya",
                        "active": true,
                        "createdAt": "2021-01-26T01:28:55.570Z",
                        "updatedAt": "2021-01-26T01:28:55.570Z",
                        "Province": {
                            "id": 21,
                            "CountryId": 1,
                            "name": "Aceh",
                            "active": true,
                            "createdAt": "2021-01-26T01:28:55.564Z",
                            "updatedAt": "2021-01-26T01:28:55.564Z",
                            "Country": {
                                "id": 1,
                                "code": "ID",
                                "name": "Indonesia",
                                "active": true,
                                "currencyCode": "IDR",
                                "createdAt": "2021-01-26T01:28:55.532Z",
                                "updatedAt": "2021-01-26T01:28:55.532Z"
                            }
                        }
                    }
                }
            },
            "domicileArea": {
                "id": 47356,
                "SuburbId": 3929,
                "name": "Pantee Rakyat",
                "postCode": "23767",
                "active": true,
                "createdAt": "2021-01-26T01:28:55.996Z",
                "updatedAt": "2021-01-26T01:28:55.996Z",
                "Suburb": {
                    "id": 3929,
                    "CityId": 256,
                    "name": "Babah Rot",
                    "active": true,
                    "createdAt": "2021-01-26T01:28:55.619Z",
                    "updatedAt": "2021-01-26T01:28:55.619Z",
                    "City": {
                        "id": 256,
                        "ProvinceId": 21,
                        "name": "Aceh Barat Daya",
                        "active": true,
                        "createdAt": "2021-01-26T01:28:55.570Z",
                        "updatedAt": "2021-01-26T01:28:55.570Z",
                        "Province": {
                            "id": 21,
                            "CountryId": 1,
                            "name": "Aceh",
                            "active": true,
                            "createdAt": "2021-01-26T01:28:55.564Z",
                            "updatedAt": "2021-01-26T01:28:55.564Z",
                            "Country": {
                                "id": 1,
                                "code": "ID",
                                "name": "Indonesia",
                                "active": true,
                                "currencyCode": "IDR",
                                "createdAt": "2021-01-26T01:28:55.532Z",
                                "updatedAt": "2021-01-26T01:28:55.532Z"
                            }
                        }
                    }
                }
            }
        },
        "RemittanceSenderRepresentative": null,
        "RemittanceRecipient": {
            "id": 735,
            "firstName": "IASDP",
            "lastName": "OOSAD",
            "gender": null,
            "dateOfBirth": null,
            "phone": null,
            "IMDestinationNumber": null,
            "idType": null,
            "idNumber": null,
            "CountryId": null,
            "ProvinceId": null,
            "CityId": null,
            "SuburbId": null,
            "AreaId": null,
            "postCode": null,
            "address": null,
            "email": null,
            "createdAt": "2021-08-12T05:10:48.911Z",
            "updatedAt": "2021-08-12T05:10:48.911Z",
            "Area": null
        },
        "RemittanceRecipientRepresentative": null,
        "PurposeOffund": {
            "id": 9,
            "name": "Family Support/Living Expenses",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:11:02.043Z",
            "updatedAt": "2021-02-14T15:11:02.043Z"
        },
        "SourceOfFund": {
            "id": 6,
            "name": "Salary / Income",
            "vendorName": "WESTERN_UNION",
            "code": null,
            "createdAt": "2021-02-14T15:05:38.196Z",
            "updatedAt": "2021-02-14T15:05:38.196Z"
        },
        "hitAccountTransactions": [],
        "DestinationCountry": {
            "id": 1,
            "code": "ID",
            "name": "Indonesia",
            "active": true,
            "currencyCode": "IDR",
            "createdAt": "2021-01-26T01:28:55.532Z",
            "updatedAt": "2021-01-26T01:28:55.532Z"
        },
        "originCountry": {
            "id": 1,
            "code": "ID",
            "name": "Indonesia",
            "active": true,
            "currencyCode": "IDR",
            "createdAt": "2021-01-26T01:28:55.532Z",
            "updatedAt": "2021-01-26T01:28:55.532Z"
        },
        "fillingDate": "08-12-21 ",
        "exchangeRates": "1.0000000",
        "inquiryToken": "Tt7YswvO8FVX+DMmRViINlUL0YoNZ8AkBE1mnuK2y7k=",
        "wuTransaction": {
            "expectedPayoutCity": "",
            "moneyTransferKey": "3623905078",
            "amount": "100,000.00",
            "destCountryCode": "ID",
            "payoutAmount": "100,000.00",
            "origDestCurrencyCode": "IDR",
            "newMtcn": "2122487989100942",
            "tolls": "0.00",
            "origCurrencyCode": "IDR",
            "payStatusDesc": "W/C",
            "filingDate": "08-12-21 ",
            "origDestCountryCode": "ID",
            "charges": "15,000.00",
            "exchangeRate": "1.0000000",
            "grossTotalAmount": "115,000.00",
            "origCountryCode": "ID",
            "filingTime": "0153A EDT",
            "principalAmount": "100,000.00",
            "destCurrencyCode": "IDR",
            "origCity": "SUBANGDO4",
            "expectedPayoutStateCode": "",
            "transactionType": "WMN"
        },
        "accountDetailClaimant": null
    },
    "response_timestamp": "2021-08-12T05:54:35.765+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Create Transaction Claim
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-wu/inquiry/claim`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
```
{
    "type": "CASH_TO_CASH_WESTERN_UNION",
    "mtcn": "7989100942",
    "transactionId": 1004,
    "claimantId": 64,
    "skipOtp": true,
    "cashier": {
        "username": "ayu.sekar"
    }
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|
**Response Success**
```
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

