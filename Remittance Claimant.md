# Remitpro - API Partner Doc. [Remittance Claimant]

|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Remittance Claimants
### Find Claimant
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/remittances/claimants?idType={idType}&idNumber={idNumber}`|`idType` & `idNumber` is required|

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "id": 67,
        "idType": "KTP",
        "idNumber": "1231231231231231",
        "idExpiryDate": "2027-06-09",
        "idImage": "https://remitpro-oss1.oss-ap-southeast-5.aliyuncs.com/upload/file_1625233032906.png",
        "customerImage": "https://remitpro-oss1.oss-ap-southeast-5.aliyuncs.com/upload/file_1625233032898.png",
        "job": "xx",
        "firstName": "ARIEF",
        "lastName": "TEST",
        "gender": "MALE",
        "dateOfBirth": "1990-06-03",
        "placeOfBirth": "BANDUNG",
        "nationality": "INDONESIA",
        "CountryId": 1,
        "ProvinceId": 21,
        "CityId": 257,
        "SuburbId": 3946,
        "AreaId": 47632,
        "address": "jalan tengah",
        "phone": "082115793644",
        "postCode": "12312",
        "domicileCountryId": 1,
        "domicileProvinceId": 21,
        "domicileCityId": 257,
        "domicileSuburbId": 3946,
        "domicileAreaId": 47632,
        "domicilePostCode": "12312",
        "domicileAddress": "jalan tengah",
        "accountNumber": "773322291239",
        "partnerToken": "335e233a28064e9551fb87a36be25db8",
        "createdAt": "2021-06-20T12:41:50.751Z",
        "updatedAt": "2021-07-02T13:37:46.479Z",
        "Area": {
            "id": 47632,
            "SuburbId": 3946,
            "name": "Tuwi Saya",
            "postCode": "23683",
            "active": true,
            "createdAt": "2021-01-26T01:28:55.996Z",
            "updatedAt": "2021-01-26T01:28:55.996Z",
            "Suburb": {
                "id": 3946,
                "CityId": 257,
                "name": "Sungai Mas",
                "active": true,
                "createdAt": "2021-01-26T01:28:55.619Z",
                "updatedAt": "2021-01-26T01:28:55.619Z",
                "City": {
                    "id": 257,
                    "ProvinceId": 21,
                    "name": "Aceh Barat",
                    "active": true,
                    "createdAt": "2021-01-26T01:28:55.570Z",
                    "updatedAt": "2021-01-26T01:28:55.570Z",
                    "Province": {
                        "id": 21,
                        "CountryId": 1,
                        "name": "Aceh",
                        "active": true,
                        "createdAt": "2021-01-26T01:28:55.564Z",
                        "updatedAt": "2021-01-26T01:28:55.564Z",
                        "Country": {
                            "id": 1,
                            "code": "ID",
                            "name": "Indonesia",
                            "active": true,
                            "currencyCode": "IDR",
                            "createdAt": "2021-01-26T01:28:55.532Z",
                            "updatedAt": "2021-01-26T01:28:55.532Z"
                        }
                    }
                }
            }
        },
        "domicileArea": {
            "id": 47632,
            "SuburbId": 3946,
            "name": "Tuwi Saya",
            "postCode": "23683",
            "active": true,
            "createdAt": "2021-01-26T01:28:55.996Z",
            "updatedAt": "2021-01-26T01:28:55.996Z",
            "Suburb": {
                "id": 3946,
                "CityId": 257,
                "name": "Sungai Mas",
                "active": true,
                "createdAt": "2021-01-26T01:28:55.619Z",
                "updatedAt": "2021-01-26T01:28:55.619Z",
                "City": {
                    "id": 257,
                    "ProvinceId": 21,
                    "name": "Aceh Barat",
                    "active": true,
                    "createdAt": "2021-01-26T01:28:55.570Z",
                    "updatedAt": "2021-01-26T01:28:55.570Z",
                    "Province": {
                        "id": 21,
                        "CountryId": 1,
                        "name": "Aceh",
                        "active": true,
                        "createdAt": "2021-01-26T01:28:55.564Z",
                        "updatedAt": "2021-01-26T01:28:55.564Z",
                        "Country": {
                            "id": 1,
                            "code": "ID",
                            "name": "Indonesia",
                            "active": true,
                            "currencyCode": "IDR",
                            "createdAt": "2021-01-26T01:28:55.532Z",
                            "updatedAt": "2021-01-26T01:28:55.532Z"
                        }
                    }
                }
            }
        }
    },
    "response_timestamp": "2021-08-02T13:31:08.354+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "ClaimantNotFound",
    "response_timestamp": "2021-08-02T13:30:19.348+00.00",
    "error": {
        "statusCode": 400,
        "message": "ClaimantNotFound",
        "error": "Bad Request"
    }
}
```

### Create Claimant
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/remittances/claimant`||

#### Request
```
{
	"idType": "KTP",
	"idNumber": "13912839183212398",
	"firstName": "apri",
	"lastName": "adi",
	"gender": "MALE",
	"dateOfBirth": "2000-06-04",
	"placeOfBirth": "bandung",
	"countryId": 1,
	"provinceId": 21,
	"cityId": 257,
	"suburbId": 3946,
	"areaId": 47632,
	"postCode": "10101",
	"address": "jakan jalan",
	"nationality": "indonesia",
	"phone": "088809512095",
	"idExpiryDate": "2025-06-04",
	"job": "wqweq",
	"domicileCountryId": 1,
	"domicileProvinceId": 21,
	"domicileCityId": 257,
	"domicileSuburbId": 3946,
	"domicileAreaId": 47632,
	"domicilePostCode": "10101",
	"domicileAddress": "jakan jalan",
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
	"response_code": 0,
	"response_message": "success",
	"response_timestamp": "2021-08-02T13:20:35.315+00.00",
	"error": null,
	"response_data": {
		"accountType": "premium",
		"idType": "KTP",
		"idNumber": "13912839183212398",
		"firstName": "apri",
		"lastName": "adi",
		"gender": "MALE",
		"dateOfBirth": "2000-06-04T00:00:00.000Z",
		"placeOfBirth": "bandung",
		"CountryId": 1,
		"ProvinceId": 21,
		"CityId": 257,
		"SuburbId": 3946,
		"AreaId": 47632,
		"postCode": "10101",
		"address": "jakan jalan",
		"nationality": "indonesia",
		"phone": "088809512095",
		"idExpiryDate": "2025-06-04T00:00:00.000Z",
		"idImage": "https://remitpro-oss1.oss-ap-southeast-5.aliyuncs.com/upload/file_1628042447629.png",
		"customerImage": "https://remitpro-oss1.oss-ap-southeast-5.aliyuncs.com/upload/file_1628042447865.png",
		"job": "wqweq",
		"domicileCountryId": 1,
		"domicileProvinceId": 21,
		"domicileCityId": 257,
		"domicileSuburbId": 3946,
		"domicileAreaId": 47632,
		"domicilePostCode": "10101",
		"domicileAddress": "jakan jalan",
		"CountryName": "ID",
		"ProvinceName": "Aceh",
		"CityName": "Aceh Barat",
		"SuburbName": "Sungai Mas",
		"AreaName": "Tuwi Saya",
		"domicileCountryName": "ID",
		"domicileProvinceName": "Aceh",
		"domicileCityName": "Aceh Barat",
		"domicileSuburbName": "Sungai Mas",
		"domicileAreaName": "Tuwi Saya",
		"id": 64,
		"accountNumber": "837330672122",
		"partnerToken": "5beb27f2b2f8ea4859df2915d91f0ca0",
		"updatedAt": "2021-08-04T02:01:04.504Z"
	}
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
