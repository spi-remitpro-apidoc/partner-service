# Remitpro - API Partner Doc. [Remittance Cash to Account]

|Environment| Host |
|----------------|-|
|Sandbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Authorization%20Token.md#remitpro-api-partner-doc-authorization)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Remittance Cash to Bank International
### Master Data 
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/transaction-cash-to-account-intl/master`|-|

#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "idType": [
            {
                "code": "1",
                "name": "CITIZENSHIP"
            },
            {
                "code": "2",
                "name": "PASSPORT"
            },
            {
                "code": "3",
                "name": "IC"
            },
            {
                "code": "4",
                "name": "DRIVING LICENSE"
            },
            {
                "code": "5",
                "name": "GREEN CARD"
            },
            {
                "code": "9",
                "name": "National ID"
            }
        ],
        "residency": [
            {
                "code": "1",
                "name": "Resident"
            },
            {
                "code": "2",
                "name": "Non Resident"
            }
        ],
        "payoutMode": [
            {
                "code": "1",
                "name": "Account Credit"
            },
            {
                "code": "2",
                "name": "Cash Pickup"
            },
            {
                "code": "3",
                "name": "Home Delivery"
            },
            {
                "code": "21",
                "name": "Union Pay"
            },
            {
                "code": "22",
                "name": "Mobile Wallet"
            },
            {
                "code": "23",
                "name": "Auxiliary Services"
            }
        ],
        "occupation": [
            {
                "code": "10",
                "name": "Businessman",
                "nameId": "Pebisnis"
            },
            {
                "code": "12",
                "name": "House Wife",
                "nameId": "Ibu Rumah Tangga"
            },
            {
                "code": "4",
                "name": "Professional",
                "nameId": "Profesional"
            },
            {
                "code": "48",
                "name": "General Worker-Agriculture",
                "nameId": "Pekerja Umum - Pertanian"
            },
            {
                "code": "49",
                "name": "General Worker-Construction",
                "nameId": "Pekerja Umum - Konstruksi"
            },
            {
                "code": "5",
                "name": "Tourist",
                "nameId": "Turis"
            },
            {
                "code": "50",
                "name": "General Worker-Manufacturing/Services/Others",
                "nameId": "Pekerja Umum - Manufaktur/Jasa/Lainnya"
            },
            {
                "code": "51",
                "name": "General Worker-Oil/Gas/Mining/Forestry",
                "nameId": "Pekerja Umum - Minyak/Gas/Pertabangan/Perhutanan"
            },
            {
                "code": "52",
                "name": "Unemployed",
                "nameId": "Tidak Bekerja"
            },
            {
                "code": "53",
                "name": "Self-employed",
                "nameId": "Wiraswasta"
            },
            {
                "code": "54",
                "name": "Student",
                "nameId": "Pelajar"
            },
            {
                "code": "55",
                "name": "Public Official/Civil Servant",
                "nameId": "Public Official/Civil Servant"
            },
            {
                "code": "56",
                "name": "Retiree/Pensioner",
                "nameId": "Pensiunan"
            },
            {
                "code": "57",
                "name": "Director",
                "nameId": "Direktur"
            },
            {
                "code": "6",
                "name": "Security Guard",
                "nameId": "Penjaga Keamanan"
            },
            {
                "code": "7",
                "name": "Supervisor",
                "nameId": "Supervisor/Pengawas"
            },
            {
                "code": "8",
                "name": "House Maid",
                "nameId": "Pekerja Rumah Tangga"
            },
            {
                "code": "9",
                "name": "Domestic Helper",
                "nameId": "Pembantu Rumah Tangga"
            }
        ],
        "relationship": [
            {
                "code": "1",
                "name": "Parent",
                "nameId": "Orang Tua"
            },
            {
                "code": "2",
                "name": "Child",
                "nameId": "Anak"
            },
            {
                "code": "3",
                "name": "Spouse",
                "nameId": "Pasangan"
            },
            {
                "code": "4",
                "name": "In laws",
                "nameId": "Dalam Hukum"
            },
            {
                "code": "5",
                "name": "Relative",
                "nameId": "Kerabat"
            },
            {
                "code": "6",
                "name": "Self",
                "nameId": "Diri Sendiri"
            },
            {
                "code": "7",
                "name": "Friend Family",
                "nameId": "Keluarga Teman"
            },
            {
                "code": "8",
                "name": "Employee Family",
                "nameId": "Keluarga Karyawan"
            },
            {
                "code": "9",
                "name": "Friend",
                "nameId": "Teman"
            },
            {
                "code": "10",
                "name": "Employee",
                "nameId": "Karyawan"
            },
            {
                "code": "11",
                "name": "Spouse Employee",
                "nameId": "Keluarga Pasangan"
            },
            {
                "code": "12",
                "name": "Spouse Supplier",
                "nameId": "Pasangan dari pemasok"
            },
            {
                "code": "13",
                "name": "Spouse Employee's Family",
                "nameId": "Pasangan dari keluarga karyawan"
            },
            {
                "code": "14",
                "name": "Supplier",
                "nameId": "Pemasok"
            },
            {
                "code": "25",
                "name": "Service Provider",
                "nameId": "Penyedia layanan"
            },
            {
                "code": "28",
                "name": "Donor/ Receiver of Charitable funds",
                "nameId": "Penerima Dana Amal"
            },
            {
                "code": "29",
                "name": "Trade/ Business Partner",
                "nameId": "Mitra Bisnis"
            }
        ],
        "sourceOfFund": [
            {
                "code": "1",
                "name": "Business Income",
                "nameId": "Pendapatan Bisnis"
            },
            {
                "code": "2",
                "name": "Monthly Income",
                "nameId": "Pendapatan Bulanan"
            },
            {
                "code": "3",
                "name": "Additional Part time Income or Commission",
                "nameId": "Tambahan Penghasilan Paruh Waktu atau Komisi"
            },
            {
                "code": "4",
                "name": "Savings",
                "nameId": "Tabungan"
            },
            {
                "code": "5",
                "name": "Loan from friends",
                "nameId": "Pinjaman dari teman"
            },
            {
                "code": "6",
                "name": "Friends Salary",
                "nameId": "Gaji Teman"
            },
            {
                "code": "7",
                "name": "Employee Salary",
                "nameId": "Gaji Karyawan"
            },
            {
                "code": "8",
                "name": "Family Income",
                "nameId": "Pendapatan Keluarga"
            },
            {
                "code": "9",
                "name": "Prizes or Winnings",
                "nameId": "Hadiah"
            },
            {
                "code": "10",
                "name": "Business Cash flow",
                "nameId": "Kas Bisnis"
            },
            {
                "code": "11",
                "name": "Spouse Income",
                "nameId": "Pendapatan pasangan"
            }
        ],
        "purposeOfRemmitance": [
            {
                "code": "13",
                "name": "Business/Investment",
                "nameId": "Bisnis/Investasi"
            },
            {
                "code": "10",
                "name": "Education",
                "nameId": "Pendidikan"
            },
            {
                "code": "3",
                "name": "Donation or Gifts",
                "nameId": "Donasi atau Hadiah"
            },
            {
                "code": "4",
                "name": "Payment of Loan",
                "nameId": "Pembayaran Pinjaman"
            },
            {
                "code": "1",
                "name": "Family Maintenance",
                "nameId": "Pemeliharaan Keluarga"
            },
            {
                "code": "6",
                "name": "Funeral Expenses",
                "nameId": "Biaya Penguburan"
            },
            {
                "code": "2",
                "name": "Household Maintenance",
                "nameId": "Pemeliharaan Rumah Tangga"
            },
            {
                "code": "7",
                "name": "Medical Expenses",
                "nameId": "Biaya Medis"
            },
            {
                "code": "9",
                "name": "Payment of bills",
                "nameId": "Pembayaran Tagihan"
            },
            {
                "code": "16",
                "name": "Payment of Goods and Services",
                "nameId": "Pembayaran Pembelian Barang dan Jasa"
            },
            {
                "code": "5",
                "name": "Purchase of Property",
                "nameId": "Pembelian Properti"
            },
            {
                "code": "15",
                "name": "Salary",
                "nameId": "Gaji"
            },
            {
                "code": "11",
                "name": "Savings",
                "nameId": "Tabungan"
            },
            {
                "code": "8",
                "name": "Wedding Expenses",
                "nameId": "Biaya Pernikahan"
            },
            {
                "code": "18",
                "name": "Personal Expenses",
                "nameId": "Biaya Pribadi"
            },
            {
                "code": "19",
                "name": "Payments of Goods",
                "nameId": "Pembayaran Pembelian Barang"
            },
            {
                "code": "20",
                "name": "Payments of Services",
                "nameId": "Pembayaran Penggunaan Jasa"
            }
        ],
        "currencyList": [
            {
                "countryCode": "AU",
                "countryName": "Australia",
                "currencyCode": "AUD",
                "currencyName": "Australian Dollar",
                "phoneCode": "+61",
                "bankPayoutModeCode": "1",
                "bankPayoutModeName": "Account Credit",
                "bankPayoutGroupCode": "AU01",
                "bankPayoutGroupName": "AUSTRALIA BANK",
                "bankPayoutAgentCode": "AU0004",
                "bankPayoutAgentName": "AUSREMIT PTY LTD",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "JP",
                "countryName": "Japan",
                "currencyCode": "JPY",
                "currencyName": "Japanese Yen",
                "phoneCode": "+81",
                "bankPayoutModeCode": "1",
                "bankPayoutModeName": "Account Credit",
                "bankPayoutGroupCode": "JP01",
                "bankPayoutGroupName": "BRASTEL BANK",
                "bankPayoutAgentCode": "JP0002",
                "bankPayoutAgentName": "BRASTEL CO LTD",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "MY",
                "countryName": "Malaysia",
                "currencyCode": "MYR",
                "currencyName": "Malaysian Ringgit",
                "phoneCode": "+61",
                "bankPayoutModeCode": "1",
                "bankPayoutModeName": "Account Credit",
                "bankPayoutGroupCode": "MY01",
                "bankPayoutGroupName": "MY BANK",
                "bankPayoutAgentCode": "MY0001",
                "bankPayoutAgentName": "MERCHANTRADE ASIA SDN BHD",
                "cashPayoutModeCode": "2",
                "cashPayoutModeName": "Cash Pickup",
                "cashPayoutGroupCode": "MY02",
                "cashPayoutGroupName": "MY CASH",
                "cashPayoutAgentCode": "MY0001",
                "cashPayoutAgentName": "MERCHANTRADE ASIA SDN BHD",
                "bank": true,
                "cash": true
            },
            {
                "countryCode": "PH",
                "countryName": "Philippines",
                "currencyCode": "PHP",
                "currencyName": "Philippine Peso",
                "phoneCode": "+63",
                "bankPayoutModeCode": "1",
                "bankPayoutModeName": "Account Credit",
                "bankPayoutGroupCode": "PH01",
                "bankPayoutGroupName": "PH BANK",
                "bankPayoutAgentCode": "PH0001",
                "bankPayoutAgentName": "IREMIT INC",
                "cashPayoutModeCode": "2",
                "cashPayoutModeName": "Cash Pickup",
                "cashPayoutGroupCode": "PH02",
                "cashPayoutGroupName": "METRO CASH",
                "cashPayoutAgentCode": "PH0003",
                "cashPayoutAgentName": "METROPOLITAN BANK & TRUST COMPANY",
                "bank": true,
                "cash": true
            },
            {
                "countryCode": "SG",
                "countryName": "Singapore",
                "currencyCode": "SGD",
                "currencyName": "Singapore Dollar",
                "phoneCode": "+65",
                "bankPayoutModeCode": "1",
                "bankPayoutModeName": "Account Credit",
                "bankPayoutGroupCode": "SG01",
                "bankPayoutGroupName": "SG BANK",
                "bankPayoutAgentCode": "SG0001",
                "bankPayoutAgentName": "AMPLE TRANSFERS PTE LTD",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "TH",
                "countryName": "Thailand",
                "currencyCode": "THB",
                "currencyName": "Thailand Baht",
                "phoneCode": "+66",
                "bankPayoutModeCode": "1",
                "bankPayoutModeName": "Account Credit",
                "bankPayoutGroupCode": "TH01",
                "bankPayoutGroupName": "TH BANK",
                "bankPayoutAgentCode": "TH0001",
                "bankPayoutAgentName": "KIATNAKIN BANK PUBLIC COMPANY LIMITED",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "UK",
                "countryName": "United Kingdom",
                "currencyCode": "GBP",
                "currencyName": "Great Britain Pound",
                "phoneCode": "+44",
                "bankPayoutModeCode": "1",
                "bankPayoutModeName": "Account Credit",
                "bankPayoutGroupCode": "UK01",
                "bankPayoutGroupName": "UK BANK",
                "bankPayoutAgentCode": "UK0004",
                "bankPayoutAgentName": "LCC TRANS-SENDING LTD UK",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "VN",
                "countryName": "Vietnam",
                "currencyCode": "VND",
                "currencyName": "Vietnamese Dong",
                "phoneCode": "+84",
                "bankPayoutModeCode": "1",
                "bankPayoutModeName": "Account Credit",
                "bankPayoutGroupCode": "VN03",
                "bankPayoutGroupName": "DONGA ACCOUNT CREDIT",
                "bankPayoutAgentCode": "VN0002",
                "bankPayoutAgentName": "DONGA MONEY TRANSFER CO LTD",
                "cashPayoutModeCode": "2",
                "cashPayoutModeName": "Cash Pickup",
                "cashPayoutGroupCode": "VN04",
                "cashPayoutGroupName": "DONGA CASH PICKUP",
                "cashPayoutAgentCode": "VN0002",
                "cashPayoutAgentName": "DONGA MONEY TRANSFER CO LTD",
                "bank": true,
                "cash": true
            },
            {
                "countryCode": "AT",
                "countryName": "Austria",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+43",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "BE",
                "countryName": "Belgium",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+32",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "CY",
                "countryName": "Cyprus",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+357",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "EE",
                "countryName": "Estonia",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+372",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "FI",
                "countryName": "Finland",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+358",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "FR",
                "countryName": "France",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+33",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "DE",
                "countryName": "Germany",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+49",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "GR",
                "countryName": "Greece",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+30",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "IE",
                "countryName": "Ireland",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+353",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "IT",
                "countryName": "Italy",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+39",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "LV",
                "countryName": "Latvia",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+371",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "LT",
                "countryName": "Lithuania",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+370",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "LU",
                "countryName": "Luxembourg",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+352",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "MT",
                "countryName": "Malta",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+356",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "NL",
                "countryName": "Netherlands",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+31",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "PT",
                "countryName": "Portugal",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+351",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "SK",
                "countryName": "Slovakia",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+421",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "SI",
                "countryName": "Slovenia",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+386",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "ES",
                "countryName": "Spain",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+34",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "BG",
                "countryName": "Bulgaria",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+359",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "HR",
                "countryName": "Croatia",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+385",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "CZ",
                "countryName": "Czech Republic",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+420",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "DK",
                "countryName": "Denmark",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+45",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "HU",
                "countryName": "Hungary",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+36",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "IS",
                "countryName": "Iceland",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+354",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "LI",
                "countryName": "Liechtenstein",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+423",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "NO",
                "countryName": "Norway",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+47",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "PL",
                "countryName": "Poland",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+48",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "RO",
                "countryName": "Romania",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+40",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "SE",
                "countryName": "Sweden",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+46",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "CH",
                "countryName": "Switzerland",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+41",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "MC",
                "countryName": "Monaco",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+377",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            },
            {
                "countryCode": "SM",
                "countryName": "San Marino",
                "currencyCode": "EUR",
                "currencyName": "European Euro",
                "phoneCode": "+378",
                "bankPayoutModeCode": "16",
                "bankPayoutModeName": "Euro Payment",
                "bankPayoutGroupCode": "MU01",
                "bankPayoutGroupName": "UNKNOWN",
                "bankPayoutAgentCode": "EU0000",
                "bankPayoutAgentName": "UNKNOWN",
                "bank": true,
                "cash": false
            }
        ],
        "transactionLimit": [
            {
                "id": 1,
                "countryCode": "AU",
                "currencyCode": "AUD",
                "bankIndividualMin": "40.000000000000",
                "bankIndividualMax": "15000.000000000000",
                "bankBusinessMin": "40.000000000000",
                "bankBusinessMax": "200000.000000000000",
                "cashIndividualMin": null,
                "cashIndividualMax": null,
                "cashBusinessMin": null,
                "cashBusinessMax": null,
                "walletIndividualMin": null,
                "walletIndividualMax": null,
                "walletBusinessMin": null,
                "walletBusinessMax": null,
                "meta": null,
                "createdAt": "2021-11-08T07:31:50.840Z",
                "updatedAt": "2021-11-08T07:31:50.840Z"
            },
            {
                "id": 2,
                "countryCode": "EU",
                "currencyCode": "EUR",
                "bankIndividualMin": null,
                "bankIndividualMax": null,
                "bankBusinessMin": null,
                "bankBusinessMax": null,
                "cashIndividualMin": null,
                "cashIndividualMax": null,
                "cashBusinessMin": null,
                "cashBusinessMax": null,
                "walletIndividualMin": null,
                "walletIndividualMax": null,
                "walletBusinessMin": null,
                "walletBusinessMax": null,
                "meta": null,
                "createdAt": "2021-11-08T07:31:50.854Z",
                "updatedAt": "2021-11-08T07:31:50.854Z"
            },
            {
                "id": 3,
                "countryCode": "JP",
                "currencyCode": "JPY",
                "bankIndividualMin": null,
                "bankIndividualMax": null,
                "bankBusinessMin": null,
                "bankBusinessMax": null,
                "cashIndividualMin": null,
                "cashIndividualMax": null,
                "cashBusinessMin": null,
                "cashBusinessMax": null,
                "walletIndividualMin": null,
                "walletIndividualMax": null,
                "walletBusinessMin": null,
                "walletBusinessMax": null,
                "meta": null,
                "createdAt": "2021-11-08T07:31:50.865Z",
                "updatedAt": "2021-11-08T07:31:50.865Z"
            },
            {
                "id": 4,
                "countryCode": "MY",
                "currencyCode": "MYR",
                "bankIndividualMin": "120.000000000000",
                "bankIndividualMax": "150000.000000000000",
                "bankBusinessMin": null,
                "bankBusinessMax": null,
                "cashIndividualMin": "120.000000000000",
                "cashIndividualMax": "25000.000000000000",
                "cashBusinessMin": null,
                "cashBusinessMax": null,
                "walletIndividualMin": "0.000000000000",
                "walletIndividualMax": "20000.000000000000",
                "walletBusinessMin": null,
                "walletBusinessMax": null,
                "meta": null,
                "createdAt": "2021-11-08T07:31:50.877Z",
                "updatedAt": "2021-11-08T07:31:50.877Z"
            },
            {
                "id": 5,
                "countryCode": "PH",
                "currencyCode": "PHP",
                "bankIndividualMin": "600.000000000000",
                "bankIndividualMax": "490000.000000000000",
                "bankBusinessMin": "600.000000000000",
                "bankBusinessMax": "490000.000000000000",
                "cashIndividualMin": "600.000000000000",
                "cashIndividualMax": "500000.000000000000",
                "cashBusinessMin": null,
                "cashBusinessMax": null,
                "walletIndividualMin": "600.000000000000",
                "walletIndividualMax": "500000.000000000000",
                "walletBusinessMin": null,
                "walletBusinessMax": null,
                "meta": null,
                "createdAt": "2021-11-08T07:31:50.888Z",
                "updatedAt": "2021-11-08T07:31:50.888Z"
            },
            {
                "id": 6,
                "countryCode": "SG",
                "currencyCode": "SGD",
                "bankIndividualMin": "50.000000000000",
                "bankIndividualMax": "20000.000000000000",
                "bankBusinessMin": "50.000000000000",
                "bankBusinessMax": "20000.000000000000",
                "cashIndividualMin": null,
                "cashIndividualMax": null,
                "cashBusinessMin": null,
                "cashBusinessMax": null,
                "walletIndividualMin": null,
                "walletIndividualMax": null,
                "walletBusinessMin": null,
                "walletBusinessMax": null,
                "meta": null,
                "createdAt": "2021-11-08T07:31:50.901Z",
                "updatedAt": "2021-11-08T07:31:50.901Z"
            },
            {
                "id": 7,
                "countryCode": "TH",
                "currencyCode": "THB",
                "bankIndividualMin": "950.000000000000",
                "bankIndividualMax": "500000.000000000000",
                "bankBusinessMin": "950.000000000000",
                "bankBusinessMax": "500000.000000000000",
                "cashIndividualMin": null,
                "cashIndividualMax": null,
                "cashBusinessMin": null,
                "cashBusinessMax": null,
                "walletIndividualMin": null,
                "walletIndividualMax": null,
                "walletBusinessMin": null,
                "walletBusinessMax": null,
                "meta": null,
                "createdAt": "2021-11-08T07:31:50.915Z",
                "updatedAt": "2021-11-08T07:31:50.915Z"
            },
            {
                "id": 8,
                "countryCode": "UK",
                "currencyCode": "GBP",
                "bankIndividualMin": "10.000000000000",
                "bankIndividualMax": "10000.000000000000",
                "bankBusinessMin": null,
                "bankBusinessMax": null,
                "cashIndividualMin": null,
                "cashIndividualMax": null,
                "cashBusinessMin": null,
                "cashBusinessMax": null,
                "walletIndividualMin": null,
                "walletIndividualMax": null,
                "walletBusinessMin": null,
                "walletBusinessMax": null,
                "meta": null,
                "createdAt": "2021-11-08T07:31:50.936Z",
                "updatedAt": "2021-11-08T07:31:50.936Z"
            },
            {
                "id": 9,
                "countryCode": "VN",
                "currencyCode": "VND",
                "bankIndividualMin": "690000.000000000000",
                "bankIndividualMax": "220000000.000000000000",
                "bankBusinessMin": null,
                "bankBusinessMax": null,
                "cashIndividualMin": "690000.000000000000",
                "cashIndividualMax": "220000000.000000000000",
                "cashBusinessMin": null,
                "cashBusinessMax": null,
                "walletIndividualMin": null,
                "walletIndividualMax": null,
                "walletBusinessMin": null,
                "walletBusinessMax": null,
                "meta": null,
                "createdAt": "2021-11-08T07:31:50.948Z",
                "updatedAt": "2021-11-08T07:31:50.948Z"
            }
        ],
        "validationData": [
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "QUEANBEYAN"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GRAFTON"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GRIFFITH"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BROKEN HILL"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ARMIDALE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "CESSNOCK"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GOULBURN"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "LISMORE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "NOWRA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BATHURST"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "DUBBO"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ORANGE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ALBURY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "CANBERRA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "TAMWORTH"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PORT MACQUARIE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "COFFS HARBOUR"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "TWEED HEADS"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MAITLAND"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WOLLONGONG"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "NEWCASTLE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "SYDNEY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WAGGA WAGGA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GOSFORD"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "LAKE MACQUARIE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WYONG"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BALLINA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BATEMANS BAY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BOWRAL MITTAGONG"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "CAMDEN HAVEN"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "FORSTER TUNCURRY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "KEMPSEY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "LITHGOW"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MORISSET COORANBONG"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MUDGEE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MUSWELLBROOK"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "NELSON BAY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PARKES"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "SINGLETON"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ST GEORGES BASIN SANCTUARY POINT"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "TAREE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ULLADULLA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WENTWORTH"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PARRAMATTA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "KELLYVILLE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "CHARTERS TOWERS"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "IPSWICH"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "LOGAN CITY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "THURINGOWA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "NAMBOUR"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GYMPIE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MOUNT ISA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MARYBOROUGH"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BUNDABERG"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "HERVEY BAY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ROCKHAMPTON"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MACKAY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "TOOWOOMBA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "CAIRNS"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "TOWNSVILLE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "SUNSHINE COAST"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GOLD COAST"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BRISBANE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "REDCLIFFE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GLADSTONE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "EMERALD"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "KINGAROY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WARWICK"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "YEPPOON"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WHYALLA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "VICTOR HARBOR"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PORT LINCOLN"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PORT PIRIE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PORT AUGUSTA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MURRAY BRIDGE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MOUNT GAMBIER"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ADELAIDE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "HOBART"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BURNIE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "CLARENCE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "DEVONPORT"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GLENORCHY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "LAUNCESTON"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ULVERSTONE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MELBOURNE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BENALLA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BALLARAT"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BENDIGO"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GEELONG"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MILDURA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "SHEPPARTON"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "SWAN HILL"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WANGARATTA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WARRNAMBOOL"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WODONGA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "LATROBE CITY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BAIRNSDALE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "COLAC"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ECHUCA MOAMA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "HORSHAM"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PORTLAND"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "SALES"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "TRARALGON MORWELL"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "WARRAGUL DROUIN"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ALBANY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BROOME"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BUNBURY"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "GERALDTON"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "KALGOORLIE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "MANDURAH"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PORT HEDLAND"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "FREMANTLE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PERTH"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "BUSSELTON"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ESPERANCE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "KARRATHA"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "DARWIN"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "KATHERINE"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "PALMERSTON"
            },
            {
                "codeType": "CITY",
                "code": "AU",
                "codeDesc": "ALICE SPRINGS"
            }
        ]
    },
    "response_timestamp": "2022-05-09T07:00:53.001+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Get Banks (Find/List/Query)
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/transaction-cash-to-account-intl/banks`|-|

#### Query Attributes
|Query |Value  |
|----------------|-|
|order| id_asc,id_desc,createdAt_asc,createdAt_desc|
|perPage| (number)|
|page| (number)|
|countryCode| (string)|
|bankCode| (string)|
|branchCode| (string)|

#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "countryCode": "MY",
            "countryName": "MALAYSIA",
            "bankCode": "1000000013",
            "bankName": "AFFIN BANK BERHAD",
            "branchCode": "3200001",
            "branchName": "AFFIN BANK HEAD OFFICE"
        },
        ...
    ],
    "response_timestamp": "2021-08-16T04:25:44.272+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Get Rate (Deprecated)
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/transaction-cash-to-account-intl/rate`|-|

#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "sourceCurrencyCode": "IDR",
            "destinationCurrencyCode": "MYR",
            "value": 0.00029
        },
        ...
    ],
    "response_timestamp": "2021-08-16T04:25:44.272+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Get Rate Active
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/transaction-cash-to-account-intl/rate/active`|`?sourceCurrencyCode=IDR&destinationCurrencyCode=MYR` using query params|

#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "id": 13,
        "sourceCurrencyCode": "IDR",
        "destinationCurrencyCode": "MYR",
        "value": "1.00",
        "meta": null,
        "createdAt": "2021-09-22T13:28:05.146Z",
        "updatedAt": "2021-09-22T13:28:05.146Z"
    },
    "response_timestamp": "2021-09-28T05:40:08.426+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Get Rate Active List
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/transaction-cash-to-account-intl/rate/active-list`|-|

#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "sourceCurrencyCode": "IDR",
            "destinationCurrencyCode": "MYR",
            "value": 0.00029
        },
        ...
    ],
    "response_timestamp": "2021-08-16T04:25:44.272+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Get Rate (Find/List/Query)
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/transaction-cash-to-account-intl/rate/list`|-|

#### Query Attributes
|Query |Value  |
|----------------|-|
|order| id_asc,id_desc,createdAt_asc,createdAt_desc|
|perPage| (number)|
|page| (number)|
|sourceCurrencyCode| (string)|
|destinationCurrencyCode| (string)|

#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "sourceCurrencyCode": "IDR",
            "destinationCurrencyCode": "MYR",
            "value": 0.00029
        },
        ...
    ],
    "response_timestamp": "2021-08-16T04:25:44.272+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Post Rate
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-account-intl/rate`|-|

#### Request
```
{
    "sourceCurrencyCode": "IDR",
    "destinationCurrencyCode": "MYR",
    "value": 0.00029
}
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "sourceCurrencyCode": "IDR",
        "destinationCurrencyCode": "MYR",
        "value": 0.00029
        
    },
    "response_timestamp": "2021-08-16T04:20:24.992+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Get Transaction Limit (Find/List/Query)
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/transaction-cash-to-account-intl/transaction-limit`|-|

#### Query Attributes
|Query |Value  |
|----------------|-|
|order| id_asc,id_desc,createdAt_asc,createdAt_desc|
|perPage| (number)|
|page| (number)|
|countryCode| (string)|

#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "countryCode": "AU",
            "currencyCode": "AUD",
            "bankIndividualMin": 40.00,
            "bankIndividualMax": 15000.00,
            "bankBusinessMin": 40.00,
            "bankBusinessMax": 200000.00,
            "cashIndividualMin": 20.00,
            "cashIndividualMax": 10000.00,
            "cashBusinessMin": 20.00,
            "cashBusinessMax": 100000.00,
            "walletIndividualMin": null,
            "walletIndividualMax": null,
            "walletBusinessMin": null,
            "walletBusinessMax": null
        },
        ...
    ],
    "response_timestamp": "2021-08-16T04:25:44.272+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Post Transaction Limit
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-account-intl/transaction-limit`|-|

#### Request

```
{
    "countryCode": "AU",
    "currencyCode": "AUD",
    "bankIndividualMin": 40.00,
    "bankIndividualMax": 15000.00,
    "bankBusinessMin": 40.00,
    "bankBusinessMax": 200000.00,
    "cashIndividualMin": 20.00,
    "cashIndividualMax": 10000.00,
    "cashBusinessMin": 20.00,
    "cashBusinessMax": 100000.00,
    "walletIndividualMin": null,
    "walletIndividualMax": null,
    "walletBusinessMin": null,
    "walletBusinessMax": null
}
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "countryCode": "AU",
        "currencyCode": "AUD",
        "bankIndividualMin": 40.00,
        "bankIndividualMax": 15000.00,
        "bankBusinessMin": 40.00,
        "bankBusinessMax": 200000.00,
        "cashIndividualMin": 20.00,
        "cashIndividualMax": 10000.00,
        "cashBusinessMin": 20.00,
        "cashBusinessMax": 100000.00,
        "walletIndividualMin": null,
        "walletIndividualMax": null,
        "walletBusinessMin": null,
        "walletBusinessMax": null
        
    },
    "response_timestamp": "2021-08-16T04:20:24.992+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Put Transaction Limit
|METHOD          |PATH |DESC  |
|----------------|-|-|
|PUT|`/api/transaction-cash-to-account-intl/transaction-limit/:countryCode`|-|

#### Request
```
/api/transaction-cash-to-account-intl/transaction-limit/AU
{
    "countryCode": "AU",
    "currencyCode": "AUD",
    "bankIndividualMin": 80.00,
    "bankIndividualMax": 30000.00,
    "bankBusinessMin": 80.00,
    "bankBusinessMax": 400000.00,
    "cashIndividualMin": 40.00,
    "cashIndividualMax": 20000.00,
    "cashBusinessMin": 40.00,
    "cashBusinessMax": 200000.00,
    "walletIndividualMin": null,
    "walletIndividualMax": null,
    "walletBusinessMin": null,
    "walletBusinessMax": null
}
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "countryCode": "AU",
        "currencyCode": "AUD",
        "bankIndividualMin": 80.00,
        "bankIndividualMax": 30000.00,
        "bankBusinessMin": 80.00,
        "bankBusinessMax": 400000.00,
        "cashIndividualMin": 40.00,
        "cashIndividualMax": 20000.00,
        "cashBusinessMin": 40.00,
        "cashBusinessMax": 200000.00,
        "walletIndividualMin": null,
        "walletIndividualMax": null,
        "walletBusinessMin": null,
        "walletBusinessMax": null
        
    },
    "response_timestamp": "2021-08-16T04:20:24.992+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Delete Transaction Limit
|METHOD          |PATH |DESC  |
|----------------|-|-|
|DELETE|`/api/transaction-cash-to-account-intl/transaction-limit/:countryCode`|-|

#### Request
```
/api/transaction-cash-to-account-intl/transaction-limit/AU
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "countryCode": "AU",
        "currencyCode": "AUD",
        "bankIndividualMin": 40.00,
        "bankIndividualMax": 15000.00,
        "bankBusinessMin": 40.00,
        "bankBusinessMax": 200000.00,
        "cashIndividualMin": 20.00,
        "cashIndividualMax": 10000.00,
        "cashBusinessMin": 20.00,
        "cashBusinessMax": 100000.00,
        "walletIndividualMin": null,
        "walletIndividualMax": null,
        "walletBusinessMin": null,
        "walletBusinessMax": null
        
    },
    "response_timestamp": "2021-08-16T04:20:24.992+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Sender Find By IdType & IdNumber
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/transaction-cash-to-account-intl/sender?idType={idType}&idNumber={idNumber}`|`idType` & `idNumber` required|

#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "id": 551,
        "idType": "KTP",
        "idNumber": "982391239182982",
        "firstName": "IAN",
        "lastName": "KASELA",
        "gender": "MALE",
        "dateOfBirth": "1994-01-01",
        "placeOfBirth": "BANDUNG",
        "CountryId": 1,
        "ProvinceId": 2,
        "CityId": 12,

        ...
    },
    "response_timestamp": "2021-08-16T04:20:24.992+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Recipient Find By IdType & IdNumber
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/transaction-cash-to-account-intl/recipient?idType={idType}&idNumber={idNumber}`|`idType` & `idNumber` required|

#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "id": 551,
        "idType": "KTP",
        "idNumber": "982391239182982",
        "firstName": "IAN",
        "lastName": "KASELA",
        "gender": "MALE",
        "dateOfBirth": "1994-01-01",
        "placeOfBirth": "BANDUNG",
        "CountryId": 1,
        "ProvinceId": 2,
        "CityId": 12,

        ...
    },
    "response_timestamp": "2021-08-16T04:20:24.992+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Sender Create
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-account-intl/sender`|-|

#### Request
|Parameter Name|Parameter Desc|Parameter Type|Status|Notes|
|:----|:----|:----|:----|:----|
|type|Service Type|string[0,32] |required|value: CASH_TO_ACCOUNT_INTL|
|idType|Sender's ID Type|string[0,32]|required|Ref : [master-data-id-type](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data) get value on attribute `name` ex: `National ID`|
|idNumber|Sender's ID Number|string[8,20]|required| |
|firstName|Sender First Name + Middle Name (if any)|string[2,50]|required| |
|lastName|Sender Last Name|string[2,50]|required| |
|gender|Sender's Gender|ENUM|required|value: ['MALE','FEMALE']|
|dateOfBirth|Sender Date of Birth|string[10,10]|required|Format YYYY-MM-DD|
|placeOfBirth|Sender Place of Birth|string[3,20]|required| |
|countryId|Sender Country ID|string[1,4]|required|Set Default : 1 ref : [master-data-country](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#countries) |
|provinceId|Sender Province ID|string[1,4]|required|ref: [master-data-province](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#province)|
|cityId|Sender City ID|string[1,4]|required|ref : [master-data-city](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#user-content-city)|
|suburbId|Sender Suburb ID|string[1,4]|required|ref : [master-data-suburb](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#sub-urb) set default value: "483" |
|areaId|Sender Area ID|string[1,4]|required|ref : [master-data-area](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#area) set default value: "4715"|
|postCode|Sender Post Code|string[5,5]|required| |
|address|Sender Address|string[0,100]|required| |
|domicileCountryId|Sender Domicile Country ID (if Sender Stay in Different Country)|string[1,4]|required|ref : [master-data-country](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#countries)|
|domicileProvinceId|Sender Domicile Province ID (if Sender Stay in Different Province)|string[1,4]|required|ref: [master-data-province](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#province)|
|domicileCityId|Sender Domicile City ID (if Sender Stay in Different City)|string[1,4]|required|ref : [master-data-city](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#user-content-city)|
|domicileSuburbId|Sender Domicile Suburb ID (if Sender Stay in Different Suburb)|string[1,4]|required|ref : [master-data-suburb](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#sub-urb) set default value: "483"|
|domicileAreaId|Sender Domicile Area ID (if Sender Stay in Different Area)|string[1,4]|required|ref : [master-data-area](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Master%20Data.md#area) set default value: "4715"|
|domicilePostCode|Sender Domicile Post Code (if Sender Stay in Different PostCode)|string[5,5]|required|set default value: "12780"|
|domicileAddress|Sender Address (if Sender Stay in Different Address)|string[0,100]|required|set default value: "Menteng Dalam"|
|phone|Sender Phone|string[5,15]|required| |
|nationality|Sender Nationality|string[2,20]|required|set default "INDONESIA"|
|bankCode|Sender Bank Code|string[3,25]|optional| |
|bankAccount|Sender Bank Account|string[3,25]|optional| |
|email|Sender Email|string[5,50]|required| |
|idPublicationDate|Sender ID Publication Date|string[10,10]|required|format YYYY-MM-DD|
|validUntilDate|Sender ID - Valid Until Date|string[10,10]|required|format YYYY-MM-DD,if Id card valid for a lifetime you can set default value: 2099-12-31|
|idCountryIssue|Sender ID Country Issue|string[10,10]|optional| |


##### Payload
```
{
    "type": "CASH_TO_ACCOUNT_INTL",
    "idType": "National ID",
    "idNumber": "1234567890123456",
    "firstName": "Antonio",
    "lastName": "Delacozta",
    "gender": "MALE",
    "dateOfBirth": "1997-05-01",
    "placeOfBirth": "BANDUNG",
    "countryId": "1",
    "provinceId": "2",
    "cityId": "12",
    "suburbId": "483",
    "areaId": "4715",
    "postCode": "123456",
    "address": "Jl Monginsidi No 187",
    "domicileCountryId": "1",
    "domicileProvinceId": "2",
    "domicileCityId": "12",
    "domicileSuburbId": "483",
    "domicileAreaId": "4715",
    "domicilePostCode": "123456",
    "domicileAddress": "Jl Monginsidi No 187",
    "phone": "+6282345678977",
    "nationality": "INDONESIA",
    "bankCode": "",
    "bankAccount": "",
    "bankBranchCode": "",
    "email": "Andelacoz@email.com",
    "idPublicationDate": "2020-01-01",
    "validUntilDate": "2099-12-31",
    "idCountryIssue": "",
    "cashier": {
      "username": "ayu.sekar"
    }
  }
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "idType": "National ID",
        "idNumber": "1234567890123456",
        "firstName": "Antonio",
        "lastName": "Delacozta",
        "gender": "MALE",
        "dateOfBirth": "1997-05-01T00:00:00.000Z",
        "placeOfBirth": "BANDUNG",
        "CountryId": 1,
        "ProvinceId": 2,
        "CityId": 12,
        "SuburbId": 483,
        "AreaId": 4715,
        "postCode": "123456",
        "address": "Jl Monginsidi No 187",
        "domicileCountryId": 1,
        "domicileProvinceId": 2,
        "domicileCityId": 12,
        "domicileSuburbId": 483,
        "domicileAreaId": 4715,
        "domicilePostCode": "123456",
        "domicileAddress": "Jl Monginsidi No 187",
        "phone": "+6282345678977",
        "nationality": "INDONESIA",
        "bankCode": null,
        "bankAccount": null,
        "email": "Andelacoz@email.com",
        "idPublicationDate": "2020-01-01T00:00:00.000Z",
        "validUntilDate": "2099-12-31T00:00:00.000Z",
        "idCountryIssue": null,
        "id": 2083,
        "createdAt": "2022-05-30T08:15:52.870Z",
        "updatedAt": "2022-05-30T08:15:52.870Z"
    },
    "response_timestamp": "2022-05-30T08:15:52.879+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```
### Recipient Create
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-account-intl/recipient`|-|

#### Request
|Parameter Name|Parameter Desc|Parameter Type|Status|Notes|
|:----|:----|:----|:----|:----|
|idType|Recipient Id Type|ENUM |required|ref: [master-data-id-type](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data), Could set default "idType" : "National ID"|
|type|Service Type|string[1,32] |required|value: CASH_TO_ACCOUNT_INTL|
|idNumber|Recipient Id Number|string[5,20]|required| |
|firstName|Recipient First Name + Middle Name (if any)|string[2,32]|required| |
|lastName|Recipient Last Name|string[2,32]|required| |
|gender|Recipient Gender|ENUM |required|value: ['MALE','FEMALE']. Could set as default - Male|
|dateOfBirth|Recipient Date of Birth|string[10,10]|required|format YYYY-MM-DD set default value: 1990-01-01|
|placeOfBirth|Recipient Place of Birth |string[2,32]|required|set default value: Sydney|
|countryId|Recipient Country Id|string[1,4]|optional| |
|provinceId|Recipient Province Id|string[1,4]|optional| |
|cityId|Recipient City Id|string[1,4]|optional| |
|suburbId|Recipient Sub Urban |string[1,4]|optional| |
|areaId|Recipient Area Id|string[1,4]|optional| |
|postCode|Recipient Postal Code|string[5,5]|optional| |
|address|Recipient Address|string[3,100]|required| |
|domicileCountryId|Recipient Country Id (if Recipient Stay in Different Country)|string[1,4]|optional| |
|domicileProvinceId|Recipient Province Id(if Recipient Stay in Different Province)|string[1,4]|optional| |
|domicileCityId|Recipient City Id (if Recipient Stay in Different City)|string[1,4]|optional| |
|domicileSuburbId|Recipient Sub Urban (if Recipient Stay in Different Sub Urban)|string[1,4]|optional| |
|domicileAreaId|Recipient Area Id (if Recipient Stay in Different Area)|string[1,4]|optional| |
|domicileAddress|Recipient Address (if Recipient Stay in Different Address)|string[3,100]|optional| |
|phone|Recipient Phone Number|string[5,15]|required|phoneCode + Phone Number,phoneCode ref to : [Object : currencyList, params : phoneCode](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data) |
|nationality|Recipient Nationality|string[2,20]|required|set default value: Australia|
|bankCode|Recipient Bank Code|string[3,25]|required|ref: [bank](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#get-banks-findlistquery)|
|bankAccount|Recipient Bank Account Number|string[3,25]|required|for - Australia Bank: Format = '6digits-[6-9digits]', Info = BSB Code +Account Number, Sample = 063739-10219898 , - Japan Bank: Format: 7-Digit ,Info: Numeric values only, Sample = 7654123 , - United Kingdom Bank: Format = 22 Characters IBAN Number, Info = Country Code + Check Digit + BIC Code + Sort Code + Account Number, Sample = GB29NWBK60161331926819 |
|bankBranchCode|Bank Branch Code|string[3,25]|required|ref: [bank](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#get-banks-findlistquery)|
|email|Recipient Email|string[3,32]|required|set default value: hello@remitpro.id|
|idPublicationDate|Recipient Id Publication Date|string[10,10] |required|format YYYY-MM-DD set default value: 2021-01-01|
|validUntilDate|Recipient Id Valid Date|string[10,10] |required|format YYYY-MM-DD set default value: 2099-12-31|
|idCountryIssue|Recipient Id Country Issue|string[10,10] |required|Format YYYY-MM-DD  set default value: 1990-01-01|
|payoutAgentCode|Bank Payout Group Code|string[1,32]|required|ref :[Object :currencyList, params : bankPayoutGroupCode](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data) |
|payoutModeCode|Mode of Payment|string[1,3] |required|set default value: 1|
|cityAU| |string[1,50]|conditional|required if destination AU ref : [Object : validationData, params :codeDesc](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data)|
|streetName| |string[1,50]|conditional|required if destination UK|
|streetType| |string[1,50]|conditional|required if destination UK|
|streetNo| |string[1,50]|conditional|required if destination UK|


##### Payload
```
{
    "type": "CASH_TO_ACCOUNT_INTL",
    "idType": "National ID",
    "idNumber": "6570000000110060",
    "firstName": "Samuel",
    "lastName": "Budiman",
    "gender": "MALE",
    "dateOfBirth": "1994-03-01",
    "placeOfBirth": "SYDNEY",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "suburbId": "",
    "areaId": "",
    "postCode": "",
    "address": "Jl. orchid forest",
    "domicileCountryId": "",
    "domicileProvinceId": "",
    "domicileCityId": "",
    "domicileSuburbId": "",
    "domicileAreaId": "",
    "domicilePostCode": "",
    "domicileAddress": "",
    "phone": "+63822332896",
    "nationality": "AUSTRALIA",
    "bankCode": "1000000008",
    "bankBranchCode": "1000001",
    "bankAccount": "110000000011002",
    "email": "hello@remitpro.id",
    "idPublicationDate": "2021-01-01",
    "validUntilDate": "2099-12-31",
    "idCountryIssue": "AUSTRALIA",
    "payoutAgentCode": "MY0001",
    "payoutModeCode": "1",
    "cityAU": "",
    "streetName":"",
    "streetName":"",
    "streetNo":"",
    "cashier": {
      "username": "ayu.sekar"
    }
  }
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "idType": "National ID",
        "idNumber": "6570000000110060",
        "firstName": "Samuel",
        "lastName": "Budiman",
        "gender": "MALE",
        "dateOfBirth": "1994-03-01T00:00:00.000Z",
        "placeOfBirth": "SYDNEY",
        "CountryId": 0,
        "ProvinceId": 0,
        "CityId": 0,
        "SuburbId": 0,
        "AreaId": 0,
        "postCode": "",
        "address": "Jl. orchid forest",
        "phone": "+63822332896",
        "nationality": "AUSTRALIA",
        "bankCode": "1000000008",
        "bankAccount": "110000000011002",
        "bankBranchCode": "1000001",
        "email": "hello@remitpro.id",
        "idPublicationDate": "2021-01-01T00:00:00.000Z",
        "validUntilDate": "2099-12-31T00:00:00.000Z",
        "idCountryIssue": "AUSTRALIA",
        "payoutAgentCode": "MY0001",
        "payoutModeCode": "1",
        "cityAU": null,
        "streetName": null,
        "streetType": null,
        "streetNo": null,
        "id": 652,
        "createdAt": "2022-05-31T00:15:05.899Z",
        "updatedAt": "2022-05-31T00:15:05.899Z"
    },
    "response_timestamp": "2022-05-31T00:15:05.913+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```
##### Payload Recipent Australia
```
{
    "type": "CASH_TO_ACCOUNT_INTL",
    "idType": "National ID",
    "idNumber": "110000000011004",
    "firstName": "Stephen D",
    "lastName": "Luffy",
    "gender": "MALE",
    "dateOfBirth": "1990-01-01",
    "placeOfBirth": "SYDNEY",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "suburbId": "",
    "areaId": "",
    "postCode": "",
    "address": "11 Gatenby Drv, Miandetta, TAS 7310",
    "domicileCountryId": "",
    "domicileProvinceId": "",
    "domicileCityId": "",
    "domicileSuburbId": "",
    "domicileAreaId": "",
    "domicilePostCode": "",
    "domicileAddress": "",
    "phone": "+6189765432324",
    "nationality": "AUSTRALIA",
    "bankCode": "AU10",
    "bankBranchCode": "AU100010",
    "bankAccount": "063739-10219898",
    "email": "hello@remitpro.id",
    "idPublicationDate": "2021-01-01",
    "validUntilDate": "2099-12-31",
    "idCountryIssue": "AUSTRALIA",
    "payoutAgentCode": "AU01",
    "payoutModeCode": "1",
    "cityAU": "CANBERRA",
    "streetName":"",
    "streetName":"",
    "streetNo":"",
    "cashier": {
      "username": "ayu.sekar"
    }
  }
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "idType": "National ID",
        "idNumber": "110000000011004",
        "firstName": "Stephen D",
        "lastName": "Luffy",
        "gender": "MALE",
        "dateOfBirth": "1990-01-01T00:00:00.000Z",
        "placeOfBirth": "SYDNEY",
        "CountryId": 0,
        "ProvinceId": 0,
        "CityId": 0,
        "SuburbId": 0,
        "AreaId": 0,
        "postCode": "",
        "address": "11 Gatenby Drv, Miandetta, TAS 7310",
        "phone": "+6189765432324",
        "nationality": "AUSTRALIA",
        "bankCode": "AU10",
        "bankAccount": "063739-10219898",
        "bankBranchCode": "AU100010",
        "email": "hello@remitpro.id",
        "idPublicationDate": "2021-01-01T00:00:00.000Z",
        "validUntilDate": "2099-12-31T00:00:00.000Z",
        "idCountryIssue": "AUSTRALIA",
        "payoutAgentCode": "AU01",
        "payoutModeCode": "1",
        "cityAU": "CANBERRA",
        "streetName": null,
        "streetType": null,
        "streetNo": null,
        "id": 654,
        "createdAt": "2022-05-31T00:15:41.952Z",
        "updatedAt": "2022-05-31T00:15:41.952Z"
    },
    "response_timestamp": "2022-05-31T00:15:42.007+00.00",
    "error": null
}

```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```
```
{
    "response_code": 999,
    "response_message": [
        {
            "name": "cityAU",
            "message": "required"
        }
    ],
    "response_timestamp": "2022-05-31T00:31:42.142+00.00",
    "error": null
}
```
##### Payload Recipent United Kingdom
```
{
    "type": "CASH_TO_ACCOUNT_INTL",
    "idType": "National ID",
    "idNumber": "110000000011089",
    "firstName": "Yuji Itadori",
    "lastName": "Tenkai",
    "gender": "MALE",
    "dateOfBirth": "1990-01-01",
    "placeOfBirth": "SYDNEY",
    "countryId": "",
    "provinceId": "",
    "cityId": "",
    "suburbId": "",
    "areaId": "",
    "postCode": "",
    "address": "Kingston Park Community Centre",
    "domicileCountryId": "",
    "domicileProvinceId": "",
    "domicileCityId": "",
    "domicileSuburbId": "",
    "domicileAreaId": "",
    "domicilePostCode": "",
    "domicileAddress": "",
    "phone": "+63822332896",
    "nationality": "AUSTRALIA",
    "bankCode": "UK01",
    "bankBranchCode": "UK010001",
    "bankAccount": "GB12345678901234567890",
    "email": "hello@remitpro.id",
    "idPublicationDate": "2021-01-01",
    "validUntilDate": "2099-12-31",
    "idCountryIssue": "AUSTRALIA",
    "payoutAgentCode": "UK01",
    "payoutModeCode": "1",
    "cityAU": "",
    "streetName":"Kingston Park Community Centre",
    "streetType":"COLOGNE",
    "streetNo":"456",
    "cashier": {
      "username": "ayu.sekar"
    }
  }
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "idType": "National ID",
        "idNumber": "110000000011089",
        "firstName": "Yuji Itadori",
        "lastName": "Tenkai",
        "gender": "MALE",
        "dateOfBirth": "1990-01-01T00:00:00.000Z",
        "placeOfBirth": "SYDNEY",
        "CountryId": 0,
        "ProvinceId": 0,
        "CityId": 0,
        "SuburbId": 0,
        "AreaId": 0,
        "postCode": "",
        "address": "Kingston Park Community Centre",
        "phone": "+63822332896",
        "nationality": "AUSTRALIA",
        "bankCode": "UK01",
        "bankAccount": "GB12345678901234567890",
        "bankBranchCode": "UK010001",
        "email": "hello@remitpro.id",
        "idPublicationDate": "2021-01-01T00:00:00.000Z",
        "validUntilDate": "2099-12-31T00:00:00.000Z",
        "idCountryIssue": "AUSTRALIA",
        "payoutAgentCode": "UK01",
        "payoutModeCode": "1",
        "cityAU": null,
        "streetName": "Kingston Park Community Centre",
        "streetType": "COLOGNE",
        "streetNo": "456",
        "id": 653,
        "createdAt": "2022-05-31T00:15:19.516Z",
        "updatedAt": "2022-05-31T00:15:19.516Z"
    },
    "response_timestamp": "2022-05-31T00:15:19.523+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```
```
{
    "response_code": 999,
    "response_message": [
        {
            "name": "streetName",
            "message": "required"
        },
        {
            "name": "streetType",
            "message": "required"
        },
        {
            "name": "streetNo",
            "message": "required"
        }
    ],
    "response_timestamp": "2022-05-31T00:35:57.093+00.00",
    "error": null
}
```
### Transaction Create
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-account-intl`|-|

#### Request
|Parameter Name|Parameter Desc|Parameter Type|Status|Notes|
|:----|:----|:----|:----|:----|
|type|Service Type|string[5,30] |required|value: CASH_TO_ACCOUNT_INTL|
|referenceId|Transaction Reference ID|string[5,50]|required|must be unique on each transaction|
|senderId|Sender ID|number |required|ref: [inquiry-sender](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#sender-find-by-idtype-idnumber) or [sender-create](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#sender-create )|
|recipientId|Recipient ID|number |required|ref: [inquiry-recipient](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#recipient-find-by-idtype-idnumber) or [recipient-create](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#recipient-create)|
|senderRelationWithReceiverCd|Sender Relation with Receiver|string[1,20]|required|ref: [master-data-relation-with-receiver](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data)|
|sourceOfFundCd|Source of Fund|string[1,20]|required|ref: [master-data-sourceof-fund](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data)|
|reasonOfRemittanceCd|Reason of Remittance|string[1,20]|required|ref: [master-data-reason-of-remittance](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data)|
|senderOccupationCd|Sender Occupation|string[1,20]|required|ref : [master-data-sender-occupation](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data)|
|senderResidencyCd|Sender Recidency|string[1,20]|required|ref : [master-data-sender-residency](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data)|
|remarks|Transaction Remarks|string[1,20]|required| |
|payoutMethod|Payout Method|string[1,10] |required|set default value : "bank"|
|currencyCode| Destination Currency Code|string[1,3] |required| |
|amountIDR|IDR Amount Transfer|string[3,12] |required| |
|countryCode|Destination Country Code|string[2,3] |required|ref: [master-data-currency-code](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#master-data)|


##### Payload
```
{
    "type": "CASH_TO_ACCOUNT_INTL",
    "referenceId": "RMT122138162313",
    "senderId": 1982,
    "recipientId": 623,
    "senderRelationWithReceiverCd": "9",
    "sourceOfFundCd": "4",
    "reasonOfRemittanceCd": "13",
    "senderOccupationCd": "4",
    "senderResidencyCd": "2",
    "remarks": "TESTC2AINTL7",
    "payoutMethod": "bank",
    "currencyCode": "AUD",
    "amountIDR": "700000",
    "cashier": {
        "username": "ayu.sekar"
    }
}
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "number": "MT20220531074211",
        "code": "1",
        "digest": "1",
        "type": "REMITTANCE_OUTBOUND",
        "metaData": {
            "SenderId": 1982,
            "RecipientId": 623,
            "payoutAmount": 65.8,
            "senderRelationWithReceiverCd": "9",
            "sourceOfFundCd": "4",
            "reasonOfRemittanceCd": "13",
            "senderOccupationCd": "4",
            "senderResidencyCd": "2",
            "remarks": "TESTC2AINTL7",
            "currencyCode": "AUD",
            "userId": 30,
            "userType": "CASHIER",
            "username": "ayu.sekar",
            "amountIDR": 700000,
            "referenceId": "RMT122138162313",
            "sourceOfFundCdName": "Savings",
            "reasonOfRemittanceCdName": "Business/Investment",
            "senderOccupationCdName": "Professional",
            "senderResidencyCdName": "Non Resident"
        },
        "mtcn": 6899083442,
        "trxRemitType": "CASH_TO_ACCOUNT_INTL",
        "remarks": "TESTC2AINTL7",
        "currencyCode": "AUD",
        "referenceId": "RMT122138162313",
        "amount": 65.8,
        "trxRates": 1.41,
        "trxTotalAmount": 67.21,
        "status": "NEW",
        "RemittanceSenderId": 1982,
        "RemittanceMerchantTradeRecipientId": 623,
        "CashierId": 30,
        "amountIDR": 700000,
        "ratesIDR": 15000,
        "totalAmountIDR": 715000,
        "exchangeRates": "0.000094",
        "id": 4879,
        "createdAt": "2022-05-31T00:42:11.710Z",
        "updatedAt": "2022-05-31T00:42:11.910Z"
    },
    "response_timestamp": "2022-05-31T00:42:11.919+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Transaction Confirm
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-account-intl/confirm`|-|

#### Request
|Parameter Name|Parameter Desc|Parameter Type|Status|Notes|
|:----|:----|:----|:----|:----|
|mtcn|Money Transfer Control Number - You'll get MTCN after create the transaction|string[8,13]|required|ref: [transaction-create](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#transaction-create )|
|cashier|Partner's operator that's doing the transaction|object() |required|example: {"username": "<cashier-username>"}|

##### Payload
```
{
    "mtcn":"6899083442",
    "cashier": {
        "username":"ayu.sekar"
    }
}
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error `error list` [error-message-list](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#error-message-list-confirm-transaction)|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "status": "COMPLETED",
        "mtcn": "6899083442",
        "vendorResponse": {
            "processRemittanceReturn": {
                "transactionID": "2100033936",
                "referenceNo": "21063784938145"
            }
        }
    },
    "response_timestamp": "2022-05-31T00:44:49.890+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```

### Transaction Inquiry
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-cash-to-account-intl/inquiry`|-|

#### Request
|Parameter Name|Parameter Desc|Parameter Type|Status|Notes|
|:----|:----|:----|:----|:----|
|mtcn|Money Transfer Control Number - You'll get MTCN after create the transaction|string[8,13]|required|ref: [transaction-create](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#transaction-create )|
|cashier|Partner's operator that's doing the transaction|object() |required|example: {"username": "<cashier-username>"}|

##### Payload
```
{
    "mtcn":"8929815333",
    "cashier": {
        "username":"ayu.sekar"
    }
}
```
#### Response
Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error `error-list` [error-message-list](https://gitlab.com/spi-remitpro-apidoc/partner-service/-/blob/master/Remittance%20Intl%20Cash%20to%20Account.md#error-message-inquiry-transaction)|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "getTransactionStatusByIdReturn": {
            "payoutAgentCd": "AU0005",
            "payoutAmount": "65.80",
            "payoutMode": "1",
            "senderIdCardTypeNo": "110000000011009",
            "receiverFirstName": "Stephen D",
            "receiverMiddleName": "",
            "receiverLastName": "Luffy",
            "receiverPhoneNo": "+6189765432324",
            "receiverBankCd": "AU10",
            "receiverBankBranchCd": "AU100010",
            "receiverBankAcNo": "063739-10219898",
            "tranDate": "5/31/2022 8:44:45 AM",
            "referenceNo": "21063784938145",
            "payoutStatus": "1",
            "payoutCurrency": "AUD",
            "txnID": "2100033936"
        }
    },
    "response_timestamp": "2022-05-31T00:45:45.744+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": null
}
```
###### Message Payout Status (Transaction Inquiry)
|code|payout status|desc|
|-|-|-|
|1	|Transaction processed |Processed |
|2	|Payment successful |Paid successful |
|7	|Transaction on Hold |On hold, pending |
|8	|Transaction Released |Released from hold |
|9	|Transaction Void |Cancelled |
|10	|Transaction Void and Refunded |Cancelled |
|17	|Transaction amended |Transaction details amended |
|18	|Sender OFAC Matched |The sender details has matched with OFAC list n it's on hold |
|19	|Receiver OFAC Matched |The receiver details has matched with OFAC list n it's on hold |
|20	|Sender OFAC released |The trxn hold due to OFAC match has been released |
|21	|Receiver OFAC released |The trxn hold due to OFAC match has been released |
|23	|Transaction Suspend |Usually applicable for Account credit trxn not credit for more than 90 days, it will be marked as Suspended at our end |
|24	|Transaction Unclaimed | Usually applicable for Cash trxn which is not claimed for more than 900 days, will be considered unclaimed at our end |

###### Error Message Inquiry Transaction
|error message|
|-|
|Internal Server Error |
|Invalid Session Id |
|Transaction Tracking Successful |
|Transaction Not found |

###### Error Message List Confirm transaction
| error message  |
|--|
|Internal Server Error|
| Invalid Session Id|
| Reference Number is Blank|
| Paying Group/Country code error|
| Reference Number Already exist|
| Paying agent code error|
| Payout amount is empty|
| Payout Amount should be greater than 0|
| Payment Mode code error|
| Unauthorized payment mode|
| Bank code empty|
| Bank Branch code empty|
| Bank A/C empty|
| Bank code error|
| Bank Branch code error|
| Bank Branch code error|
| Sender Last Name empty|
| Sender Nationality empty|
| Sender Nationality code error|
| Paying Agent not found |
| Paying Group not valid |
| Charge rule not exist |
| No Forex Tag Defined. |
| Invalid sender birth date |
| Sender birth date is empty |
| Reason of remittance is empty |
| Source of fund is empty |
| Deposit amount is empty |
| Invalid deposit amount |
| Service charge is empty |
| Invalid service charge |
| GST amount empty |
| Invalid GST amount |
| Text for reason of remittance is empty when selected as others|
| Text for source of fund is empty when selected as others |
| Invalid discount amount |
| Limit checking  |
| Country code error |
| Invalid Remit Type |
| Invalid Country of Business |
| Registration date of Company Empty |
| Remit Type Empty |
| Invalid authorized person id card type |
| Invalid authorized person nationality code |
| Invalid authorized person Date of Birth |
| Invalid Nature of Business|
| Payment mode not allowed|
| Paying Agent not valid|
| Paying Agent not valid|
| Insufficient balance|
| Token validation|
| Incorrect pay|out amount format|
| Remarks field is empty.|
| Required Field is missing.|
| Input field Validation failed.|
| Invalid Processid.|
