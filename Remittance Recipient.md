# Remitpro - API Partner Doc. [Remittance Recipient]

|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Remittance Recipient
### Create Recipient
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/remittances/recipient`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
|Field |Status  | Desc
|-|-|-|
|type|required||
|firstName|required||
|lastName|required||
|dateOfBirth|optional||
|phone|optional||
|idType|conditional|`required` if type is `CASH_TO_CASH_BRI`|
|idNumber|conditional|`required` if type is `CASH_TO_CASH_BRI`|
|countryId|conditional|`required` if type is `CASH_TO_CASH_BRI`|
|provinceId|conditional|`required` if type is `CASH_TO_CASH_BRI`|
|cityId|conditional|`required` if type is `CASH_TO_CASH_BRI`|
|suburbId|conditional|`required` if type is `CASH_TO_CASH_BRI`|
|areaId|conditional|`required` if type is `CASH_TO_CASH_BRI`|
|address|conditional|`required` if type is `CASH_TO_CASH_BRI`|
|postCode|optional||
|email|optional||

**Payload***
```
{
    "type": "CASH_TO_CASH_REMITPRO",
    "firstName":"aisdo",
    "lastName":"sdsp",
    "dateOfBirth": "1990-01-01",
    "phone":"021",
    "idType":"KTP",
    "idNumber":"123123123",
    "countryId": "1",
    "provinceId": "2",
    "cityId": "12",
    "suburbId": "72",
    "areaId": "835",
    "postCode":"33242",
    "address":"jalan bandung",
    "email":"mail@mail.com"
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "firstName": "aisdo",
        "lastName": "sdsp",
        "dataOfBirth": "",
        "phone": "021",
        "idType": "KTP",
        "idNumber": "123123123",
        "CountryId": 1,
        "ProvinceId": 2,
        "CityId": 12,
        "SuburbId": 72,
        "AreaId": 835,
        "address": "jalan bandung",
        "id": 642,
        "createdAt": "2021-08-02T13:47:40.132Z",
        "updatedAt": "2021-08-02T13:47:40.132Z"
    },
    "response_timestamp": "2021-08-02T13:47:40.359+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
