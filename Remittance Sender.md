# Remitpro - API Partner Doc. [Remittance Sender]

|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Remittance Sender
### Find Sender
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/remittances/senders?idType={idType}&idNumber={idNumber}`|`idType` & `idNumber` is required|

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "id": 222,
        "idType": "KTP",
        "idNumber": "1231231231231231",
        "firstName": "ARIEF",
        "lastName": "HDIAYA",
        "gender": "MALE",
        "dateOfBirth": "1997-06-04",
        "placeOfBirth": "BANDUNG",
        "CountryId": 1,
        "ProvinceId": 2,
        "CityId": 12,
        "SuburbId": 78,
        "AreaId": 858,
        "postCode": "33517",
        "address": "JALAN",
        "domicileCountryId": 1,
        "domicileProvinceId": 2,
        "domicileCityId": 12,
        "domicileSuburbId": 78,
        "domicileAreaId": 858,
        "domicilePostCode": "33517",
        "domicileAddress": "JALAN",
        "phone": "6288809512095",
        "nationality": "INDONESIA",
        "bankCode": null,
        "bankAccount": null,
        "idPublicationDate": null,
        "validUntilDate": null,
        "relationToReceiver": null,
        "email": null,
        "employmentStatus": null,
        "employmentPosition": null,
        "employmentIndustry": null,
        "IMSenderIdNumber": "5c958e90-357e-4a0f-b47b-b945be27a359",
        "idCountryIssue": null,
        "createdAt": "2021-06-19T05:19:37.717Z",
        "updatedAt": "2021-07-01T17:38:58.047Z",
        "Area": {
            "id": 858,
            "SuburbId": 78,
            "name": "Kelubi",
            "postCode": "33517",
            "active": true,
            "createdAt": "2021-01-26T01:28:56.003Z",
            "updatedAt": "2021-01-26T01:28:56.003Z",
            "Suburb": {
                "id": 78,
                "CityId": 12,
                "name": "Manggar",
                "active": true,
                "createdAt": "2021-01-26T01:28:55.619Z",
                "updatedAt": "2021-01-26T01:28:55.619Z",
                "City": {
                    "id": 12,
                    "ProvinceId": 2,
                    "name": "Belitung Timur",
                    "active": true,
                    "createdAt": "2021-01-26T01:28:55.570Z",
                    "updatedAt": "2021-01-26T01:28:55.570Z",
                    "Province": {
                        "id": 2,
                        "CountryId": 1,
                        "name": "Bangka Belitung",
                        "active": true,
                        "createdAt": "2021-01-26T01:28:55.564Z",
                        "updatedAt": "2021-01-26T01:28:55.564Z",
                        "Country": {
                            "id": 1,
                            "code": "ID",
                            "name": "Indonesia",
                            "active": true,
                            "currencyCode": "IDR",
                            "createdAt": "2021-01-26T01:28:55.532Z",
                            "updatedAt": "2021-01-26T01:28:55.532Z"
                        }
                    }
                }
            }
        },
        "domicileArea": {
            "id": 858,
            "SuburbId": 78,
            "name": "Kelubi",
            "postCode": "33517",
            "active": true,
            "createdAt": "2021-01-26T01:28:56.003Z",
            "updatedAt": "2021-01-26T01:28:56.003Z",
            "Suburb": {
                "id": 78,
                "CityId": 12,
                "name": "Manggar",
                "active": true,
                "createdAt": "2021-01-26T01:28:55.619Z",
                "updatedAt": "2021-01-26T01:28:55.619Z",
                "City": {
                    "id": 12,
                    "ProvinceId": 2,
                    "name": "Belitung Timur",
                    "active": true,
                    "createdAt": "2021-01-26T01:28:55.570Z",
                    "updatedAt": "2021-01-26T01:28:55.570Z",
                    "Province": {
                        "id": 2,
                        "CountryId": 1,
                        "name": "Bangka Belitung",
                        "active": true,
                        "createdAt": "2021-01-26T01:28:55.564Z",
                        "updatedAt": "2021-01-26T01:28:55.564Z",
                        "Country": {
                            "id": 1,
                            "code": "ID",
                            "name": "Indonesia",
                            "active": true,
                            "currencyCode": "IDR",
                            "createdAt": "2021-01-26T01:28:55.532Z",
                            "updatedAt": "2021-01-26T01:28:55.532Z"
                        }
                    }
                }
            }
        }
    },
    "response_timestamp": "2021-08-02T13:22:37.070+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "SenderNotFound",
    "response_timestamp": "2021-08-02T13:20:35.315+00.00",
    "error": {
        "statusCode": 400,
        "message": "SenderNotFound",
        "error": "Bad Request"
    }
}
```

### Create Sender
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/remittances/sender`||

#### Headers
|Key          |Value |DESC  |
|----------------|-|-|
|`Content-Type`|`application/json`||

#### Request
|Field |Status  | Desc
|-|-|-|
|type|required||
|idType|required||
|idNumber|required||
|firstName|required||
|lastName|required||
|gender|required||
|dateOfBirth|required||
|placeOfBirth|required||
|countryId|required||
|provinceId|required||
|cityId|required||
|suburbId|required||
|areaId|required||
|postCode|required||
|address|required||
|domicileCountryId|required||
|domicileProvinceId|required||
|domicileCityId|required||
|domicilePostCode|required||
|domicileAddress|required||
|phone|required||
|nationality|required||
|email|required||
|idPublicationDate|required||
|validUntilDate|required||
|relationToReceiver|optional||
|employmentStatus|optional||
|employmentIndustry|optional||
|idCountryIssue|optional||

#### Request
```
{
    "type": "CASH_TO_CASH_REMITPRO",
    "idType": "KTP",
    "idNumber": "982391239182982",
    "firstName": "IAN",
    "lastName": "KASELA",
    "gender": "MALE",
    "dateOfBirth": "1994-01-01",
    "placeOfBirth": "BANDUNG",
    "countryId": "1",
    "provinceId": "2",
    "cityId": "12",
    "suburbId": "72",
    "areaId": "835",
    "postCode": "33572",
    "address": "KAMPIT",
    "domicileCountryId": "1",
    "domicileProvinceId": "2",
    "domicileCityId": "12",
    "domicileSuburbId": "72",
    "domicileAreaId": "835",
    "domicilePostCode": "33572",
    "domicileAddress": "KAMPIT",
    "phone": "0888829238921",
    "nationality": "WNI",
    "bankCode": null,
    "bankAccount": null,
    "email": "test1232@mail.com",
    "idPublicationDate": "2021-12-12",
    "validUntilDate": "2029-12-12",
    "relationToReceiver": "Family",
    "employmentStatus": "Employed",
    "employmentPosition": "Entry Level",
    "employmentIndustry": "Construction",
    "idCountryIssue": "1990-01-01"
}
```

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "idType": "KTP",
        "idNumber": "982391239182982",
        "firstName": "IAN",
        "lastName": "KASELA",
        "gender": "MALE",
        "dateOfBirth": "1994-01-01T00:00:00.000Z",
        "placeOfBirth": "BANDUNG",
        "CountryId": 1,
        "ProvinceId": 2,
        "CityId": 12,
        "SuburbId": 72,
        "AreaId": 835,
        "postCode": "33572",
        "address": "KAMPIT",
        "domicileCountryId": 1,
        "domicileProvinceId": 2,
        "domicileCityId": 12,
        "domicileSuburbId": 72,
        "domicileAreaId": 835,
        "domicilePostCode": "33572",
        "domicileAddress": "KAMPIT",
        "phone": "0888829238921",
        "nationality": "WNI",
        "bankCode": null,
        "bankAccount": null,
        "email": "test1232@mail.com",
        "idPublicationDate": "2021-12-12T00:00:00.000Z",
        "validUntilDate": "2029-12-12T00:00:00.000Z",
        "relationToReceiver": "Family",
        "employmentStatus": "Employed",
        "employmentPosition": "Entry Level",
        "employmentIndustry": "Construction",
        "idCountryIssue": "1990-01-01",
        "id": 601,
        "createdAt": "2021-08-02T13:24:27.876Z",
        "updatedAt": "2021-08-02T13:24:27.876Z"
    },
    "response_timestamp": "2021-08-02T13:24:28.506+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
