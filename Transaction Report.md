# Remitpro - API Partner Doc. [Transaction Report]

|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Transaction Report
### Transaction Status by MTCN
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-report/inquiry-transaction`||

#### Request
|field|type |DESC  |
|-|-|-|
|type|ENUM|`required`|
|mtcn|string|`optional`|
|reffId|string|`optional` only for `CASH_TO_ACCOUNT` type|
|cashier|object|`required`|

*** Request By Mtcn ****
```
{
    "type": "CASH_TO_CASH_REMITPRO",
    "mtcn": "5357570056",
    "cashier": {
        "username": "ayu.sekar"
    }
}
```
*** Request By Reff Id ****
```
{
    "type": "CASH_TO_ACCOUNT",
    "reffId": "TestBRI1231123123",
    "cashier": {
        "username": "ayu.sekar"
    }
}
```

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "transactionId": 2660,
        "trxRemitType": "CASH_TO_CASH_REMITPRO",
        "type": "REMITTANCE_OUTBOUND",
        "mtcn": "5357570056",
        "referenceId": "1",
        "number": "20211109132005",
        "status": "COMPLETED",
        "isClaim": false
    },
    "response_timestamp": "2021-11-29T08:00:08.030+00.00",
    "error": null
}
```

**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

## Reporting
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/transaction-report`||

#### Request
|field|type |DESC  |
|-|-|-|
|type|ENUM|`required`|
|page|number|`required`|
|perPage|number|`required`|
|from|string|`required`. ex: `2021-10-11`|
|to|string|`required`. ex: `2021-10-11`|
|cashier|object|`required`|
|branchId|string|`optional`|
|cashierId|string|`conditional`|

```
{
    "page": 1,
    "perPage": 10,
    "from": "2021-10-01",
    "to": "2021-10-15",
    "type": "CASH_TO_CASH_REMITPRO",
    "cashier": {
        "username": "ayu.sekar"
    },
    "branchId": 33,
    "cashierId": 18
}
```

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "pagination": {
            "totalData": 19,
            "currentData": 10,
            "total": 2,
            "current": 1,
            "next": 2,
            "prev": null
        },
        "data": [
            {
                "id": 1732,
                "number": "20211015153857",
                "type": "REMITTANCE_OUTBOUND",
                "trxRemitType": "CASH_TO_ACCOUNT",
                "amount": "12345.00",
                "cashierId": 30,
                "status": "PENDING",
                "mtcn": "1692330570",
                "referenceId": "20211015153857X1692330570",
                "sender": {
                    "id": 222,
                    "idType": "KTP",
                    "idNumber": "1231231231231231",
                    "firstName": "ARIEF",
                    "lastName": "HDIAYA",
                    "gender": "MALE",
                    "dateOfBirth": "1997-06-04",
                    "placeOfBirth": "BANDUNG",
                    "countryOfBirth": "Indonesia",
                    "CountryId": 1,
                    "ProvinceId": 2,
                    "CityId": 12,
                    "SuburbId": 78,
                    "AreaId": 858,
                    "postCode": "33517",
                    "address": "JALAN",
                    "domicileCountryId": 1,
                    "domicileProvinceId": 2,
                    "domicileCityId": 12,
                    "domicileSuburbId": 78,
                    "domicileAreaId": 858,
                    "domicilePostCode": "33517",
                    "domicileAddress": "JALAN",
                    "phone": "6288809512095",
                    "nationality": "INDONESIA",
                    "bankCode": null,
                    "bankAccount": null,
                    "idPublicationDate": null,
                    "validUntilDate": null,
                    "relationToReceiver": "Family",
                    "email": null,
                    "employmentStatus": "Employed",
                    "employmentPosition": "Entry Level",
                    "employmentIndustry": "Agriculture/Manufacturing",
                    "IMSenderIdNumber": "5c958e90-357e-4a0f-b47b-b945be27a359",
                    "idCountryIssue": null,
                    "createdAt": "2021-06-19T05:19:37.717Z",
                    "updatedAt": "2021-11-24T03:10:12.027Z",
                    "idImage": null
                },
                "cashier": {
                    "id": 30,
                    "firstName": "ayu",
                    "lastName": "sekar",
                    "accountNumber": "346812373332",
                    "phone": "09876644362",
                    "email": "remitlkd1+5@gmail.com",
                    "username": "ayu.sekar",
                    "Branch": {
                        "id": 24
                    }
                },
                "bank": "BCA",
                "bankCode": "BCA",
                "bankAccount": "999999",
                "bankAccountName": null
            },
            ....
        ]
    },
    "response_timestamp": "2021-11-29T08:23:14.654+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
