# Remitpro - API Partner Doc. [Virtual Account]


|Environtment| Host |
|----------------|-|
|Sanbox|`https://apigw-devel.kaspro.id/remitpro-api-partner-v2`|
|Production|`https://apigw.kaspro.id/remitpro-api-partner-v2`|

**Header Value**
> Required every request

|KEY          |VALUE |DESC  |
|----------------|-|-|
|Authorization|`Bearer {access_token}`|`access_token` [Click Here](https://gitlab.com/spi-remitpro/api-documentation/partner-service/-/blob/master/Authorization%20Token.md)|
|Kaspro-Signature|`{signature}`|-|
|Date|`{dateRequest}`|timestamp utc +0 (iso string)|
|partnerKey|`{partnerkey}`|-|

## Virtual Account Remittance
### Get Banks
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/api/virtual-account/banks`||

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": [
        {
            "id": 1,
            "status": "active",
            "originType": "bank",
            "originId": "bri",
            "bankCode": "BRI",
            "shortCode": "BRI",
            "code": "BRI",
            "name": "Bank Rakyat Indonesia",
            "usage": "va",
            "meta": null,
            "createdAt": "2021-10-04T10:37:46.974Z",
            "updatedAt": "2021-10-04T10:37:46.974Z"
        },
        {
            "id": 2,
            "status": "active",
            "originType": "bank",
            "originId": "bni",
            "bankCode": "BNI",
            "shortCode": "BNI",
            "code": "BNI",
            "name": "Bank Negara Indonesia",
            "usage": "va",
            "meta": null,
            "createdAt": "2021-10-04T10:37:46.988Z",
            "updatedAt": "2021-10-04T10:37:46.988Z"
        },
        {
            "id": 3,
            "status": "active",
            "originType": "bank",
            "originId": "bca",
            "bankCode": "BCA",
            "shortCode": "BCA",
            "code": "BCA",
            "name": "Bank Central Asia",
            "usage": "va",
            "meta": null,
            "createdAt": "2021-10-04T10:37:47.001Z",
            "updatedAt": "2021-10-04T10:37:47.001Z"
        },

        ...
    ],
    "response_timestamp": "2021-08-02T13:07:18.116+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Get Virtual Account Status
|METHOD          |PATH |DESC  |
|----------------|-|-|
|GET|`/xfers/virtual-account/:identifier`||
**Request**
```
identifier: transaction contract origin id OR virtual account number
```

#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "status": "pending",
        "ownerId": "",
        "merchantCode": "",
        "accountNumber": "7777731500705825",
        "externalId": "REMITPRO7854380499",
        "bankCode": "BRI",
        "shortCode": "BRI",
        "code": "BRI",
        "name": "REMITPRO VA",
        "suffixNumber": "705825",
        "virtualAccountNumber": "7777731500705825",
        "suggestedAmount": 100000,
        "expectedAmount": 100000,
        "expirationDate": "2021-10-20T14:36:52.996Z",
        "isClosed": true,
        "isSingleUse": true,
        "originType": "payment",
        "originId": "contract_0341b7d949ff4be3a48eec297e2a7271",
        "originStatus": "pending",
        "originExpiredAt": "2021-10-20T14:36:52.996Z",
        "paymentMethodType": "virtual_bank_account",
        "paymentMethodId": "va_9f47bf84cd7fd563b7af68591b7a970a",
        "referenceId": "REMITPRO7854380499",
        "bankShortCode": "BRI",
        "description": "KIRIM DUIT",
        "reqDisplayName": "REMITPRO VA",
        "resDisplayName": "IKN-REMITPRO VA",
        "suffixNo": "705825",
        "accountNo": "7777731500705825",
        "amount": 100000,
        "fee": 3630,
        "request": null,
        "response": null,
        "callback": null,
        "meta": null,
        "id": 6,
        "createdAt": "2021-10-10T14:36:53.916Z",
        "updatedAt": "2021-10-10T14:36:53.916Z"
    },
    "response_timestamp": "2021-10-10T14:36:55.342+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```

### Create Virtual Account
|METHOD          |PATH |DESC  |
|----------------|-|-|
|POST|`/api/virtual-account/create-payment`||

**Request**
```
{
    "bankCode": "BRI",
    "description": "KIRIM DUIT",
    "displayName": "REMITPRO VA",
    "amount": 100000,
    "transactionId": 1637,
    "cashier": {
        "username": "ayu.sekar"
    }
}
```
#### Response
|Response Code |DESC  |
|----------------|-|
|0| Successs|
|999| Error|

**Response Success**
```
{
    "response_code": 0,
    "response_message": "success",
    "response_data": {
        "status": "pending",
        "ownerId": "",
        "merchantCode": "",
        "accountNumber": "7777731500705825",
        "externalId": "REMITPRO7854380499",
        "bankCode": "BRI",
        "shortCode": "BRI",
        "code": "BRI",
        "name": "REMITPRO VA",
        "suffixNumber": "705825",
        "virtualAccountNumber": "7777731500705825",
        "suggestedAmount": 100000,
        "expectedAmount": 100000,
        "expirationDate": "2021-10-20T14:36:52.996Z",
        "isClosed": true,
        "isSingleUse": true,
        "originType": "payment",
        "originId": "contract_0341b7d949ff4be3a48eec297e2a7271",
        "originStatus": "pending",
        "originExpiredAt": "2021-10-20T14:36:52.996Z",
        "paymentMethodType": "virtual_bank_account",
        "paymentMethodId": "va_9f47bf84cd7fd563b7af68591b7a970a",
        "referenceId": "REMITPRO7854380499",
        "bankShortCode": "BRI",
        "description": "KIRIM DUIT",
        "reqDisplayName": "REMITPRO VA",
        "resDisplayName": "IKN-REMITPRO VA",
        "suffixNo": "705825",
        "accountNo": "7777731500705825",
        "amount": 100000,
        "fee": 3630,
        "request": {
            "data": {
                "attributes": {
                    "paymentMethodType": "virtual_bank_account",
                    "amount": 100000,
                    "referenceId": "REMITPRO7854380499",
                    "expiredAt": "2021-10-20T14:36:52.996Z",
                    "description": "REMITPRO VA",
                    "paymentMethodOptions": {
                        "bankShortCode": "BRI",
                        "displayName": "REMITPRO VA",
                        "suffixNo": "705825"
                    }
                }
            }
        },
        "response": {
            "data": {
                "attributes": {
                    "paymentMethodType": "virtual_bank_account",
                    "amount": 100000,
                    "referenceId": "REMITPRO7854380499",
                    "expiredAt": "2021-10-20T14:36:52.996Z",
                    "description": "REMITPRO VA",
                    "paymentMethodOptions": {
                        "bankShortCode": "BRI",
                        "displayName": "REMITPRO VA",
                        "suffixNo": "705825"
                    }
                }
            }
        },
        "callback": null,
        "meta": null,
        "id": 6,
        "createdAt": "2021-10-10T14:36:53.916Z",
        "updatedAt": "2021-10-10T14:36:53.916Z"
    },
    "response_timestamp": "2021-10-10T14:36:55.342+00.00",
    "error": null
}
```
**Response Error**
```
{
    "response_code": 999,
    "response_message": "Internal server error",
    "response_timestamp": "2021-07-28T05:24:08.877+00.00",
    "error": {
        "statusCode": 500,
        "message": "Internal server error"
    }
}
```
